-- phpMyAdmin SQL Dump
-- version 3.5.8.2
-- http://www.phpmyadmin.net
--
-- Host: sql102.byetcluster.com
-- Generation Time: May 12, 2015 at 12:49 PM
-- Server version: 5.6.22-71.0
-- PHP Version: 5.3.3

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `b31_13867109_mydb`
--

-- --------------------------------------------------------

--
-- Table structure for table `clients`
--

DROP TABLE IF EXISTS `clients`;
CREATE TABLE IF NOT EXISTS `clients` (
  `ClientID` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Client ID (PK)',
  `ClientCompanyName` varchar(45) DEFAULT NULL COMMENT 'Client company name',
  `ClientTelephone` varchar(45) DEFAULT NULL COMMENT 'Client contact number',
  `ClientContactPerson` varchar(45) DEFAULT NULL COMMENT 'Client contact person',
  `ClientDiscount` int(11) DEFAULT NULL COMMENT 'Client discount rate',
  `ClientQuota` float DEFAULT NULL COMMENT 'Client quota limit',
  `ClientStatus` int(11) DEFAULT NULL COMMENT 'Client status',
  `Address` varchar(100) DEFAULT NULL,
  `DeliveryTeam` varchar(16) DEFAULT NULL COMMENT 'Delivery Team',
  PRIMARY KEY (`ClientID`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COMMENT='Client record, ClientID is the primary key' AUTO_INCREMENT=8 ;

--
-- Dumping data for table `clients`
--

INSERT INTO `clients` (`ClientID`, `ClientCompanyName`, `ClientTelephone`, `ClientContactPerson`, `ClientDiscount`, `ClientQuota`, `ClientStatus`, `Address`, `DeliveryTeam`) VALUES
(1, 'Queen Mary Hospital', '29657563', 'Otto', 2, 200, 1, 'Pok Fu Lam, Hong Kong Island, Hong Kong', 'Team A'),
(2, 'Tung Wah Eastern Hospital', '23687452', 'Jacob', 5, 2000, 1, 'So Kon Po, Causeway Bay, Hong Kong', 'Team A'),
(3, 'Queen Elizabeth Hospital', '27863142', 'Larry', 10, 4000, 0, 'King''s Park,, Kowloon, Hong Kong', 'Team B'),
(4, 'Polly', '32432423', 'Polly', 5, 500, 2, 'king''s road', 'Team A'),
(5, 'Peter', '234234', 'Peter', 6, 5002, 1, 'test address 123', 'Team A'),
(6, 'Nam Long Hospital', '29030000', 'Kenny', 0, 0, 1, '30 Nam Long Shan Rd, Wong Chuk Hang', 'Team B'),
(7, 'Tuen Mun Hospital', '24685111', 'Jason', 0, 0, 1, 'Address, 23 Tsing Chung Koon Road, Tuen Mun, NT.', 'Team B');

-- --------------------------------------------------------

--
-- Table structure for table `deliveryhdr`
--

DROP TABLE IF EXISTS `deliveryhdr`;
CREATE TABLE IF NOT EXISTS `deliveryhdr` (
  `DeliveryID` int(11) NOT NULL DEFAULT '0' COMMENT 'Delivery ID (PK)',
  `OrderID` int(11) NOT NULL COMMENT 'Order ID (FK)',
  `ClientID` int(11) NOT NULL COMMENT 'Client ID (FK)',
  `LocationID` int(11) NOT NULL COMMENT 'Location ID (FK)',
  `EmployeeID` int(11) NOT NULL COMMENT 'Employee ID (FK)',
  `DeliveryCreateTime` datetime DEFAULT NULL COMMENT 'Create time',
  `DeliveryLastEditTime` datetime DEFAULT NULL COMMENT 'Last modify time',
  `DeliveryExpectDate` datetime DEFAULT NULL COMMENT 'Exprect delivery time',
  `DeliveryTotalPrice` float DEFAULT NULL COMMENT 'Delivery total price',
  PRIMARY KEY (`DeliveryID`),
  KEY `fk_deliveryhdr_order1_idx` (`OrderID`),
  KEY `fk_deliveryhdr_clients1_idx` (`ClientID`),
  KEY `fk_deliveryhdr_locations1_idx` (`LocationID`),
  KEY `fk_deliveryhdr_employees1_idx` (`EmployeeID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='Delivery note header, DeliveryID is the primary key, OrderID is foreign key linked to Orders table, ClientID is foreign key linked to Clients table, LocationID is foreign key linked to Locations table, DeliveryEmployeeID is foreign key linked to Employees table';

-- --------------------------------------------------------

--
-- Table structure for table `deliveryline`
--

DROP TABLE IF EXISTS `deliveryline`;
CREATE TABLE IF NOT EXISTS `deliveryline` (
  `DeliveryLineID` int(11) NOT NULL COMMENT 'Delivery ID (PK)',
  `DeliveryID` int(11) NOT NULL COMMENT 'Delivery ID (FK)',
  `MedicalID` int(11) NOT NULL COMMENT 'Medical ID (FK)',
  `LotID` int(11) NOT NULL COMMENT 'Lot ID (FK)',
  `DeliveryLineQty` int(11) DEFAULT NULL COMMENT 'Quantity',
  `DeliveryLineItemPrice` float DEFAULT NULL COMMENT 'Line item amount',
  PRIMARY KEY (`DeliveryLineID`),
  KEY `fk_deliveryline_deliveryhdr1_idx` (`DeliveryID`),
  KEY `fk_deliveryline_medical1_idx` (`MedicalID`),
  KEY `fk_deliveryline_lots1_idx` (`LotID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='Delivery detail, DeliveryLineID is the primary key of DeliveryLine, Medicines is foreign key linked to Medicines table';

-- --------------------------------------------------------

--
-- Table structure for table `employees`
--

DROP TABLE IF EXISTS `employees`;
CREATE TABLE IF NOT EXISTS `employees` (
  `EmployeeID` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Employee ID (PK)',
  `EmployeeFirstName` varchar(45) DEFAULT NULL COMMENT 'Employee first name',
  `EmployeeLastName` varchar(45) DEFAULT NULL COMMENT 'Employee last name',
  `EmployeeTitle` varchar(45) DEFAULT NULL COMMENT 'Employee Title',
  `Login` varchar(45) DEFAULT NULL COMMENT 'Login name',
  `Password` varchar(45) DEFAULT NULL COMMENT 'Password',
  `EmployeeStatus` int(11) DEFAULT NULL COMMENT 'Employee status',
  `Gender` int(1) NOT NULL,
  `Phone` varchar(8) NOT NULL,
  `Address` varchar(100) NOT NULL,
  `Email` varchar(50) NOT NULL,
  `Salary` int(9) NOT NULL,
  `DateOfBirth` date NOT NULL,
  PRIMARY KEY (`EmployeeID`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COMMENT='Employees record, EmployeeID is the primary key of Employees table' AUTO_INCREMENT=4 ;

--
-- Dumping data for table `employees`
--

INSERT INTO `employees` (`EmployeeID`, `EmployeeFirstName`, `EmployeeLastName`, `EmployeeTitle`, `Login`, `Password`, `EmployeeStatus`, `Gender`, `Phone`, `Address`, `Email`, `Salary`, `DateOfBirth`) VALUES
(1, 'Admin', 'Super', 'Site admin', 'admin', '21232f297a57a5a743894a0e4a801fc3', 1, 1, '88885555', 'HK', 'tom@gmail.com', 110001, '0000-00-00'),
(2, NULL, NULL, 'sdf', 'Paul', NULL, 23, 1, '324234', 'safsdf', 'asf@adf.c', 234234, '0000-00-00');

-- --------------------------------------------------------

--
-- Table structure for table `locations`
--

DROP TABLE IF EXISTS `locations`;
CREATE TABLE IF NOT EXISTS `locations` (
  `LocationID` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Location ID (PK)',
  `ClientID` int(11) NOT NULL COMMENT 'Client ID (FK)',
  `LocationFacilityName` varchar(45) DEFAULT NULL COMMENT 'Location description',
  `LocationFloor` varchar(45) DEFAULT NULL COMMENT 'Location floor',
  `LocationWingNo` varchar(45) DEFAULT NULL COMMENT 'Location wing number',
  `LocationBuilding` varchar(255) DEFAULT NULL COMMENT 'Location building',
  `LocationRoad` varchar(45) DEFAULT NULL COMMENT 'Location road',
  `LocationCountry` varchar(45) DEFAULT NULL COMMENT 'Location country',
  `LocationStatus` int(11) DEFAULT NULL COMMENT 'Location status',
  PRIMARY KEY (`LocationID`),
  KEY `fk_locations_clients1_idx` (`ClientID`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COMMENT='Location detail for clients, LocationID is the primary key of Locations table, ClientID is foreign key linked to Clients table' AUTO_INCREMENT=4 ;

--
-- Dumping data for table `locations`
--

INSERT INTO `locations` (`LocationID`, `ClientID`, `LocationFacilityName`, `LocationFloor`, `LocationWingNo`, `LocationBuilding`, `LocationRoad`, `LocationCountry`, `LocationStatus`) VALUES
(1, 1, 'Union Hospital', '', NULL, 'LG/F,logistic Department,1 union road, kwun tong', '', NULL, NULL),
(2, 2, 'Tseng Kwun O Hospital', NULL, NULL, '2/F,Logistic Department, 2 choi ming st,tseng kwun O', NULL, NULL, NULL),
(3, 3, 'Elisablish Hosptial', NULL, NULL, '1/F, 123 golf road, jordan', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `lots`
--

DROP TABLE IF EXISTS `lots`;
CREATE TABLE IF NOT EXISTS `lots` (
  `LotID` int(11) NOT NULL COMMENT 'Lot ID (PK)',
  `MedicalID` int(11) NOT NULL COMMENT 'Medical ID (FK)',
  `LotsQty` int(11) DEFAULT NULL COMMENT 'Quantity',
  `LotsExpireDate` date DEFAULT NULL COMMENT 'Expire Date',
  PRIMARY KEY (`LotID`),
  KEY `fk_lots_medical1_idx` (`MedicalID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='Lot record for medicines, LotID is the primary key of Lots table, MedicineID is foreign key linked to Medicines table';

--
-- Dumping data for table `lots`
--

INSERT INTO `lots` (`LotID`, `MedicalID`, `LotsQty`, `LotsExpireDate`) VALUES
(120140530, 1, 20, '2013-12-03'),
(120140917, 1, 70, '2013-12-04'),
(120140918, 2, 70, '2013-12-04'),
(120140919, 2, 70, '2013-12-04'),
(120140920, 3, 70, '2013-12-04'),
(120140921, 3, 70, '2013-12-04'),
(120140922, 4, 70, '2013-12-04'),
(120140923, 4, 70, '2013-12-04'),
(120140924, 5, 70, '2013-12-04'),
(120140925, 5, 70, '2013-12-04'),
(120140926, 6, 70, '2013-12-04'),
(120140927, 6, 70, '2013-12-04'),
(120140928, 7, 70, '2013-12-04'),
(120140929, 7, 70, '2013-12-04');

-- --------------------------------------------------------

--
-- Table structure for table `medical`
--

DROP TABLE IF EXISTS `medical`;
CREATE TABLE IF NOT EXISTS `medical` (
  `MedicalID` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Medical ID (PK)',
  `MedicalName` varchar(45) DEFAULT NULL COMMENT 'Medical name',
  `MedicalDescription` varchar(45) DEFAULT NULL COMMENT 'Medical description',
  `MedicalPrice` float DEFAULT NULL COMMENT 'Medical price',
  `MedicalRetailPrice` float DEFAULT NULL COMMENT 'Medicine retail price',
  `MedicalStatus` int(11) DEFAULT NULL COMMENT 'Medicine status',
  `MedicalTotalQty` int(11) DEFAULT NULL COMMENT 'Medical total quantity',
  `MedicalType` varchar(45) DEFAULT NULL COMMENT 'Drag type',
  PRIMARY KEY (`MedicalID`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COMMENT='Item details record, MedicineID is the primary key of Item table' AUTO_INCREMENT=10 ;

--
-- Dumping data for table `medical`
--

INSERT INTO `medical` (`MedicalID`, `MedicalName`, `MedicalDescription`, `MedicalPrice`, `MedicalRetailPrice`, `MedicalStatus`, `MedicalTotalQty`, `MedicalType`) VALUES
(1, 'Robitussin', 'Cough up', 15, 20, 1, 200, '7569841'),
(2, 'Tylenol', 'Cold', 18, 25, 1, 50, '7569841'),
(3, 'Sudafed', 'Sensitive', 19, 45, 0, 50, '7569841'),
(4, 'Panadol', 'Panadol', 10, 20, 1, 50, '2378652'),
(5, 'Anti-cancer drugs', 'Anti-cancer drugs', 50, 80, 1, 150, '2378652'),
(6, 'Childrens Motrin', 'Children fever', 12, 25, 1, 600, '7569841'),
(7, 'Day Quil', 'Ease pain', 10, 20, 1, 600, '7569841'),
(8, 'Aspirin', 'Aspirin', 23, 43, 1, 343, NULL),
(9, 'Glucobay', 'Glucobay', 34, 56, 1, 234, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `order`
--

DROP TABLE IF EXISTS `order`;
CREATE TABLE IF NOT EXISTS `order` (
  `OrderID` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Order ID (PK)',
  `EmployeeID` int(11) NOT NULL COMMENT 'Employee ID (FK)',
  `ClientID` int(11) NOT NULL COMMENT 'Client ID (FK)',
  `LocationID` int(11) NOT NULL COMMENT 'Location ID (FK)',
  `PaymentID` int(11) DEFAULT NULL COMMENT 'Order payment ID (FK)',
  `OrderPackingEmpID` int(11) DEFAULT NULL COMMENT 'Employee ID for packing (FK)',
  `OrderFinalCheckEmpID` int(11) DEFAULT NULL COMMENT 'Employee ID for final check (FK)',
  `OrderCreateTime` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Create time',
  `OrderLastEditTime` date DEFAULT NULL COMMENT 'Last modify time',
  `OrderExpectDeliveryDate` date DEFAULT NULL COMMENT 'Expect delivery time',
  `OrderStatus` int(11) DEFAULT NULL COMMENT 'Order status (0: cancel; 1:complete; 2:inactive)',
  `OrderTotalPrice` float DEFAULT NULL COMMENT 'Order tatal price',
  PRIMARY KEY (`OrderID`),
  KEY `fk_order_employees1_idx` (`EmployeeID`),
  KEY `fk_order_clients1_idx` (`ClientID`),
  KEY `fk_order_locations1_idx` (`LocationID`),
  KEY `fk_order_payment1_idx` (`PaymentID`),
  KEY `fk_order_employees2_idx` (`OrderPackingEmpID`),
  KEY `fk_order_employees3_idx` (`OrderFinalCheckEmpID`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COMMENT='Order details, OrderID is the primary key of OrderHdr table, ClientID is foreign key linked to Clients table, LocationID is foreign key linked to Locations table, EmployeeID is foreign key linked to Employees table' AUTO_INCREMENT=105 ;

--
-- Dumping data for table `order`
--

INSERT INTO `order` (`OrderID`, `EmployeeID`, `ClientID`, `LocationID`, `PaymentID`, `OrderPackingEmpID`, `OrderFinalCheckEmpID`, `OrderCreateTime`, `OrderLastEditTime`, `OrderExpectDeliveryDate`, `OrderStatus`, `OrderTotalPrice`) VALUES
(104, 0, 3, 0, NULL, NULL, NULL, '2013-12-18 11:25:46', NULL, NULL, NULL, NULL),
(103, 0, 7, 0, NULL, NULL, NULL, '2013-12-17 18:22:27', NULL, NULL, NULL, NULL),
(102, 0, 1, 0, NULL, NULL, NULL, '2013-12-17 17:51:04', NULL, NULL, NULL, NULL),
(101, 0, 7, 0, NULL, NULL, NULL, '2013-12-17 17:39:52', NULL, NULL, NULL, NULL),
(100, 0, 6, 0, NULL, NULL, NULL, '2013-12-17 17:37:25', NULL, NULL, NULL, NULL),
(99, 0, 6, 0, NULL, NULL, NULL, '2013-12-17 17:23:44', NULL, NULL, NULL, NULL),
(98, 0, 7, 0, NULL, NULL, NULL, '2013-12-17 17:21:25', NULL, NULL, 1, NULL),
(97, 0, 6, 0, NULL, NULL, NULL, '2013-12-17 14:30:35', NULL, NULL, NULL, NULL),
(96, 0, 1, 0, NULL, NULL, NULL, '2013-12-16 19:28:00', NULL, NULL, NULL, NULL),
(95, 0, 2, 0, NULL, NULL, NULL, '2013-12-16 19:25:15', NULL, NULL, 2, NULL),
(94, 0, 3, 0, NULL, NULL, NULL, '2013-12-16 19:23:38', NULL, NULL, 1, NULL),
(93, 0, 3, 0, NULL, NULL, NULL, '2013-12-16 19:16:58', NULL, NULL, NULL, NULL),
(92, 0, 4, 0, NULL, NULL, NULL, '2013-12-16 19:16:02', NULL, NULL, 1, NULL),
(91, 0, 1, 0, NULL, NULL, NULL, '2013-12-16 19:13:40', NULL, NULL, 1, NULL),
(90, 0, 2, 0, NULL, NULL, NULL, '2013-12-16 16:51:37', NULL, NULL, NULL, NULL),
(89, 0, 5, 0, NULL, NULL, NULL, '2013-12-16 16:48:25', NULL, NULL, 1, NULL),
(88, 0, 4, 0, NULL, NULL, NULL, '2013-12-16 11:57:58', NULL, NULL, NULL, NULL),
(87, 0, 3, 0, NULL, NULL, NULL, '2013-12-15 20:44:30', NULL, NULL, 2, NULL),
(86, 0, 1, 0, NULL, NULL, NULL, '2013-12-15 18:48:03', NULL, NULL, 1, NULL),
(85, 0, 1, 0, NULL, NULL, NULL, '2013-12-15 18:08:27', NULL, NULL, NULL, NULL),
(82, 0, 1, 0, NULL, NULL, NULL, '2013-12-15 15:20:48', NULL, NULL, 1, NULL),
(83, 0, 1, 0, NULL, NULL, NULL, '2013-12-15 17:48:25', NULL, NULL, 1, NULL),
(84, 0, 4, 0, NULL, NULL, NULL, '2013-12-15 17:49:46', NULL, NULL, 2, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `patient`
--

DROP TABLE IF EXISTS `patient`;
CREATE TABLE IF NOT EXISTS `patient` (
  `PatientID` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Patient ID (PK)',
  `ClientID` int(11) NOT NULL COMMENT 'Client ID (FK)',
  `LocationID` int(11) NOT NULL COMMENT 'Patient location (FK)',
  `PatientFirstName` varchar(45) DEFAULT NULL COMMENT 'Patient first name',
  `PatientLastName` varchar(45) DEFAULT NULL COMMENT 'Patient last name',
  `PatientGender` int(11) DEFAULT NULL COMMENT 'Patient gender',
  `PatientDateOfBirth` date DEFAULT NULL COMMENT 'Patient date of birth',
  `PatientRemark` varchar(45) DEFAULT NULL COMMENT 'Patient remark',
  `PatientContact` varchar(45) DEFAULT NULL COMMENT 'Patient contact',
  `PatientRoom` varchar(45) DEFAULT NULL COMMENT 'Patient room number',
  `PatientAddress` varchar(45) DEFAULT NULL COMMENT 'Patient address',
  `PatientStatus` int(11) DEFAULT NULL COMMENT 'Patient status',
  PRIMARY KEY (`PatientID`),
  KEY `fk_patient_clients1_idx` (`ClientID`),
  KEY `fk_patient_locations1_idx` (`LocationID`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COMMENT='Keep patient record, patientID is the primary key of Patient table, clientID is foreign key linked to Clients table, LocationID is foreign key linked to Locations table' AUTO_INCREMENT=4 ;

--
-- Dumping data for table `patient`
--

INSERT INTO `patient` (`PatientID`, `ClientID`, `LocationID`, `PatientFirstName`, `PatientLastName`, `PatientGender`, `PatientDateOfBirth`, `PatientRemark`, `PatientContact`, `PatientRoom`, `PatientAddress`, `PatientStatus`) VALUES
(1, 1, 1, 'Veronica', 'Fung', 0, '2013-11-05', 'xxx', '77778888', '4401', 'HK', 1),
(2, 2, 2, 'Raymond', 'Chan', 1, '2013-11-13', 'ccc', '77779999', '3403', 'KL', 0),
(3, 3, 3, 'Rita', 'Tse', 0, '2013-11-13', NULL, '77779999', '1011', 'KL', 1);

-- --------------------------------------------------------

--
-- Table structure for table `payment`
--

DROP TABLE IF EXISTS `payment`;
CREATE TABLE IF NOT EXISTS `payment` (
  `PaymentID` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Payment ID (PK)',
  `ClientID` int(11) NOT NULL COMMENT 'Client ID (FK)',
  `EmployeeID` int(11) NOT NULL COMMENT 'EmployeeID (FK)',
  `PrescriptionID` int(11) NOT NULL COMMENT 'Prescription ID (FK)',
  `PaymentCreateTime` datetime DEFAULT CURRENT_TIMESTAMP COMMENT 'Payment create time',
  `PaymentLastEditTime` datetime DEFAULT NULL COMMENT 'Payment last modify time',
  `PaymentType` int(11) DEFAULT NULL COMMENT 'Payment type',
  `PaymentAmount` float DEFAULT NULL COMMENT 'Payment amount',
  `OrderID` int(11) NOT NULL,
  PRIMARY KEY (`PaymentID`),
  KEY `fk_payment_clients1_idx` (`ClientID`),
  KEY `fk_payment_employees1_idx` (`EmployeeID`),
  KEY `fk_payment_prescription1_idx` (`PrescriptionID`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COMMENT='Keep payment transaction record, PaymentID is the primary key, ClientID is foreign key linked to Clients table, EmployeeID is foreign key linked to Employees table' AUTO_INCREMENT=13 ;

--
-- Dumping data for table `payment`
--

INSERT INTO `payment` (`PaymentID`, `ClientID`, `EmployeeID`, `PrescriptionID`, `PaymentCreateTime`, `PaymentLastEditTime`, `PaymentType`, `PaymentAmount`, `OrderID`) VALUES
(1, 1, 0, 47, '2013-11-05 00:00:00', '2013-11-20 00:00:00', 1, 250, 47),
(2, 2, 0, 48, '2013-11-05 00:00:00', '2013-11-05 00:00:00', 1, 250, 48),
(3, 3, 0, 49, '2013-11-05 00:00:00', '2013-11-05 00:00:00', 2, 800, 49),
(4, 3, 0, 0, '2013-12-16 00:00:00', NULL, 2, 1001, 82),
(5, 3, 0, 0, '2013-12-16 11:54:31', NULL, 1, 1212, 82),
(6, 2, 0, 0, '2013-12-16 12:06:43', NULL, 1, 2001, 90),
(7, 5, 0, 0, '2013-12-16 12:30:12', NULL, 2, 123, 89),
(8, 1, 0, 0, '2013-12-16 14:06:56', NULL, 2, 123, 82),
(9, 3, 0, 0, '2013-12-16 14:17:54', NULL, 1, 112, 93),
(10, 1, 0, 0, '2013-12-16 14:29:29', NULL, 2, 2333, 96),
(11, 4, 0, 0, '2013-12-17 08:10:53', NULL, 2, 500, 88),
(12, 7, 0, 0, '2013-12-18 03:06:20', NULL, 2, 100, 103);

-- --------------------------------------------------------

--
-- Table structure for table `pohdr`
--

DROP TABLE IF EXISTS `pohdr`;
CREATE TABLE IF NOT EXISTS `pohdr` (
  `POID` int(11) NOT NULL COMMENT 'PO ID (PK)',
  `SupplierID` int(11) NOT NULL COMMENT 'Supplier ID (FK)',
  `EmployeeID` int(11) NOT NULL COMMENT 'Employee ID (FK)',
  `POCreateTime` datetime DEFAULT NULL COMMENT 'Create Time',
  `POLastEditTime` datetime DEFAULT NULL COMMENT 'Last modify time',
  `POExpectDelivery` datetime DEFAULT NULL COMMENT 'Expected delivery time',
  `POTotal` float DEFAULT NULL COMMENT 'Total amount of the PO',
  `POStatus` int(11) DEFAULT NULL COMMENT 'PO status',
  PRIMARY KEY (`POID`),
  KEY `fk_pohdr_suppliers1_idx` (`SupplierID`),
  KEY `fk_pohdr_employees1_idx` (`EmployeeID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='Purchase order header record, POHDR_ID is the primary key of POhdr table, SupplierID is foreign key linked to Suppliers table, EmployeeID is foreign key linked to Employees table';

--
-- Dumping data for table `pohdr`
--

INSERT INTO `pohdr` (`POID`, `SupplierID`, `EmployeeID`, `POCreateTime`, `POLastEditTime`, `POExpectDelivery`, `POTotal`, `POStatus`) VALUES
(1, 1, 0, '2013-11-04 00:00:00', '2013-11-05 00:00:00', '2013-11-18 00:00:00', 20, 2),
(2, 2, 0, '2013-11-04 00:00:00', '2013-11-05 00:00:00', '2013-11-19 00:00:00', 22, 2),
(3, 3, 0, '2013-11-05 00:00:00', '2013-11-06 00:00:00', '2013-11-19 00:00:00', 25, 1);

-- --------------------------------------------------------

--
-- Table structure for table `poline`
--

DROP TABLE IF EXISTS `poline`;
CREATE TABLE IF NOT EXISTS `poline` (
  `POLineID` int(11) NOT NULL COMMENT 'PO Line ID (PK)',
  `POID` int(11) NOT NULL COMMENT 'PO ID (FK)',
  `MedicalID` int(11) NOT NULL COMMENT 'PO item id (FK)',
  `POLinePrice` float DEFAULT NULL COMMENT 'Purchase line price',
  `POLineQty` int(11) DEFAULT NULL COMMENT 'Purchase line quility',
  PRIMARY KEY (`POLineID`),
  KEY `fk_poline_pohdr1_idx` (`POID`),
  KEY `fk_poline_medical1_idx` (`MedicalID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='Purchase order detail record, LineID is the primary key of POLine table, POID is foreign key linked to POHdr table';

--
-- Dumping data for table `poline`
--

INSERT INTO `poline` (`POLineID`, `POID`, `MedicalID`, `POLinePrice`, `POLineQty`) VALUES
(1, 1, 1, 10, 200),
(2, 1, 2, 12, 50),
(3, 2, 1, 10, 200),
(4, 2, 2, 12, 50);

-- --------------------------------------------------------

--
-- Table structure for table `prescription`
--

DROP TABLE IF EXISTS `prescription`;
CREATE TABLE IF NOT EXISTS `prescription` (
  `PrescriptionID` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Prescription ID (PK)',
  `OrderID` int(11) NOT NULL COMMENT 'Order ID (FK)',
  `PatientID` int(11) NOT NULL COMMENT 'Patient ID (FK)',
  PRIMARY KEY (`PrescriptionID`),
  KEY `fk_prescription_order1_idx` (`OrderID`),
  KEY `fk_prescription_patient1_idx` (`PatientID`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COMMENT='Prescriptionrecord, PrescriptionID is the primary key of Prescription table, OrderID is foreign key linked to Order table, PatientID is foreign key linked to Patient table' AUTO_INCREMENT=54 ;

--
-- Dumping data for table `prescription`
--

INSERT INTO `prescription` (`PrescriptionID`, `OrderID`, `PatientID`) VALUES
(35, 88, 2),
(34, 87, 2),
(33, 0, 1),
(32, 82, 1),
(31, 88, 3),
(30, 82, 2),
(29, 86, 3),
(28, 0, 3),
(27, 83, 1),
(26, 83, 2),
(25, 83, 3),
(24, 82, 3),
(23, 82, 3),
(22, 82, 1),
(21, 85, 1),
(20, 82, 3),
(17, 50, 2),
(18, 83, 1),
(19, 84, 3),
(36, 88, 1),
(37, 88, 3),
(38, 88, 3),
(39, 82, 3),
(40, 88, 3),
(41, 0, 1),
(42, 89, 3),
(43, 90, 1),
(44, 0, 3),
(45, 93, 2),
(46, 95, 1),
(47, 96, 2),
(48, 88, 2),
(49, 97, 2),
(50, 98, 3),
(51, 103, 3),
(52, 103, 2),
(53, 104, 2);

-- --------------------------------------------------------

--
-- Table structure for table `prescriptionline`
--

DROP TABLE IF EXISTS `prescriptionline`;
CREATE TABLE IF NOT EXISTS `prescriptionline` (
  `PrescriptionLineID` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Prescription line ID (PK)',
  `PrescriptionID` int(11) NOT NULL COMMENT 'Prescription ID (FK)',
  `MedicalID` int(11) NOT NULL COMMENT 'Medical ID (FK)',
  `PrescriptionLineQty` int(11) DEFAULT NULL COMMENT 'Quantity',
  `PrescriptionLineMethod` varchar(45) DEFAULT NULL COMMENT 'Prescription Method',
  `LotID` int(11) NOT NULL,
  PRIMARY KEY (`PrescriptionLineID`),
  KEY `fk_prescriptionline_prescription1_idx` (`PrescriptionID`),
  KEY `fk_prescriptionline_medical1_idx` (`MedicalID`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COMMENT='Prescription line detail, PrescriptionLineID is the primary key of PrescriptionLine, PrescriptionID is foreign key to Prescription, MedicalID is foreign key linked to Medicines table' AUTO_INCREMENT=52 ;

--
-- Dumping data for table `prescriptionline`
--

INSERT INTO `prescriptionline` (`PrescriptionLineID`, `PrescriptionID`, `MedicalID`, `PrescriptionLineQty`, `PrescriptionLineMethod`, `LotID`) VALUES
(21, 23, 4, 1, 'eat', 2324),
(20, 18, 2, 1, 'eat', 234),
(19, 0, 1, 123, 'eat', 2342),
(18, 20, 3, 1, 'eat', 2342),
(17, 23, 3, 1, 'eat', 23423),
(16, 20, 4, 123, 'eat', 234242),
(15, 21, 3, 123, 'eat', 234234),
(14, 20, 4, 12, 'eat', 32424),
(11, 17, 7, 342, 'swallow', 23424),
(12, 17, 4, 1, 'eat', 324242),
(13, 18, 1, 11, 'drink', 234423),
(22, 0, 2, 2, 'eat', 0),
(23, 18, 3, 0, '', 0),
(24, 18, 2, 2342, 'eat', 2342),
(25, 18, 1, 123, '', 0),
(26, 18, 4, 0, 'eat', 0),
(27, 18, 4, 0, '', 0),
(28, 18, 7, 1111, 'eat', 123),
(29, 20, 8, 1231, 'eat', 2342),
(30, 27, 7, 123, 'eat', 2342),
(31, 28, 7, 100, 'drink', 122342),
(32, 29, 6, 111, 'eat', 12123),
(33, 29, 4, 11, 'eat', 23424),
(34, 25, 1, 123, 'eat', 234),
(35, 31, 2, 11, 'eat', 123),
(36, 31, 7, 5, 'eat', 2342),
(37, 32, 1, 1, '', 123456),
(38, 31, 3, 111, 'eat', 2342),
(39, 40, 5, 12, 'eat', 234),
(40, 42, 5, 12, 'eat', 234),
(41, 43, 4, 1, 'eat', 234),
(42, 20, 3, 23, 'eat', 3432),
(43, 45, 2, 123, 'eat', 234),
(44, 47, 3, 16, 'eat', 324),
(45, 42, 5, 10, 'Eating', 98745),
(46, 26, 8, 2, 'Eat', 963175),
(47, 49, 7, 2, 'Eat', 933175),
(48, 50, 3, 12, 'eat', 234234),
(49, 51, 4, 100, 'eat', 3423),
(50, 53, 6, 3, 'Eat', 976541),
(51, 53, 1, 3, 'Eat', 7863021);

-- --------------------------------------------------------

--
-- Table structure for table `receivehdr`
--

DROP TABLE IF EXISTS `receivehdr`;
CREATE TABLE IF NOT EXISTS `receivehdr` (
  `ReceiveHDRID` int(11) NOT NULL COMMENT 'Receive ID (PK)',
  `POID` int(11) NOT NULL COMMENT 'Purchase order ID (FK)',
  `EmployeeID` int(11) NOT NULL COMMENT 'Employee ID (FK)',
  `SupplierID` int(11) NOT NULL COMMENT 'Supplier ID (FK)',
  `ReceiveCreateTime` datetime DEFAULT NULL COMMENT 'Create time',
  `ReceiveLastEditTime` datetime DEFAULT NULL COMMENT 'Last modify time',
  `ReceiveTotalAmount` float DEFAULT NULL COMMENT 'Receive total amount',
  PRIMARY KEY (`ReceiveHDRID`),
  KEY `fk_receivehdr_employees1_idx` (`EmployeeID`),
  KEY `fk_receivehdr_suppliers1_idx` (`SupplierID`),
  KEY `fk_receivehdr_pohdr1_idx` (`POID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='Receive header that link information of order receive together, ReceiveID is the primary key of ReceiveHDR, POID is foreign key linked to PO';

--
-- Dumping data for table `receivehdr`
--

INSERT INTO `receivehdr` (`ReceiveHDRID`, `POID`, `EmployeeID`, `SupplierID`, `ReceiveCreateTime`, `ReceiveLastEditTime`, `ReceiveTotalAmount`) VALUES
(1, 1, 0, 1, '2013-12-04 00:00:00', '2013-12-18 00:00:00', 11),
(2, 2, 0, 2, '2013-12-09 00:00:00', '2013-12-27 00:00:00', 12),
(3, 3, 0, 3, '2013-12-23 00:00:00', '2013-12-31 00:00:00', 13);

-- --------------------------------------------------------

--
-- Table structure for table `receiveline`
--

DROP TABLE IF EXISTS `receiveline`;
CREATE TABLE IF NOT EXISTS `receiveline` (
  `ReceiveLineID` int(11) NOT NULL COMMENT 'Receive line ID (PK)',
  `medical_MedicalID` int(11) NOT NULL COMMENT 'Receive medical ID (FK)',
  `receivehdr_ReceiveHDRID` int(11) NOT NULL COMMENT 'Receive header ID (FK)',
  `ReceiveLinePrice` float DEFAULT NULL COMMENT 'Delivery note total price',
  `ReceiveLineQty` int(11) DEFAULT NULL COMMENT 'Receive line quitily',
  `ReceiveLotNo` int(11) DEFAULT NULL COMMENT 'Lot Number of receive line',
  PRIMARY KEY (`ReceiveLineID`),
  KEY `fk_receiveline_medical1_idx` (`medical_MedicalID`),
  KEY `fk_receiveline_receivehdr1_idx` (`receivehdr_ReceiveHDRID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='Delivery detail records and total price of receive line, ReceiveLineID is the primary key of ReceiveLine, LineID is foreign key linked to POLine table';

--
-- Dumping data for table `receiveline`
--

INSERT INTO `receiveline` (`ReceiveLineID`, `medical_MedicalID`, `receivehdr_ReceiveHDRID`, `ReceiveLinePrice`, `ReceiveLineQty`, `ReceiveLotNo`) VALUES
(1, 1, 1, 300, 10, 23234),
(2, 2, 1, 1300, 10, 44344),
(3, 1, 2, 300, 10, 23234),
(4, 2, 2, 1300, 10, 44344);

-- --------------------------------------------------------

--
-- Table structure for table `role`
--

DROP TABLE IF EXISTS `role`;
CREATE TABLE IF NOT EXISTS `role` (
  `RoleID` int(11) NOT NULL COMMENT 'Role ID (PK)',
  `EmployeeID` int(11) NOT NULL COMMENT 'Employee ID (FK)',
  `RoleName` varchar(45) DEFAULT NULL COMMENT 'Role name',
  `RoleDescription` varchar(45) DEFAULT NULL COMMENT 'Role description',
  PRIMARY KEY (`RoleID`),
  KEY `fk_role_employees_idx` (`EmployeeID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='For assign permission and restrict access to the system. RoleID is primary key of Role table, EmployeeID is foreign key linked to Employees table.';

-- --------------------------------------------------------

--
-- Table structure for table `suppliers`
--

DROP TABLE IF EXISTS `suppliers`;
CREATE TABLE IF NOT EXISTS `suppliers` (
  `SupplierID` int(11) NOT NULL COMMENT 'Supplier ID (PK)',
  `SupplierContactPerson` varchar(45) DEFAULT NULL COMMENT 'Supplier contact person',
  `SupplierTel` varchar(45) DEFAULT NULL COMMENT 'Supplier telephone',
  `SupplierAddress` varchar(45) DEFAULT NULL COMMENT 'Supplier address',
  `SupplierStatus` varchar(45) DEFAULT NULL COMMENT 'Supplier status',
  `SupplierName` varchar(64) NOT NULL,
  PRIMARY KEY (`SupplierID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='Keep supplier record and contact detail, supplierID is the primary key of Suppliers';

--
-- Dumping data for table `suppliers`
--

INSERT INTO `suppliers` (`SupplierID`, `SupplierContactPerson`, `SupplierTel`, `SupplierAddress`, `SupplierStatus`, `SupplierName`) VALUES
(1, 'alex', '5436563', '333 road', '1', 'Dragon Company'),
(2, 'ben', '34535345', 'abc road hk', '0', 'Potenial Company'),
(3, 'cat', '5645643', 'def road hk', '1', 'Golden Company');

-- --------------------------------------------------------

--
-- Table structure for table `test2`
--

DROP TABLE IF EXISTS `test2`;
CREATE TABLE IF NOT EXISTS `test2` (
  `t` int(11) NOT NULL,
  `e` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COMMENT='test';

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
