<?php
ini_set( 'date.timezone', 'Asia/Hong_Kong' );

include("class.php");

	//==============================================================================================
	if(!isset($_SESSION['page']))
	{
		$_SESSION['page']="login";
	} else if(!isset($_GET['js'])) {
		$_SESSION['js']=$_SERVER["REQUEST_TIME"];
		//echo $_SESSION['page'];
	}
	
	if(!isset($_SESSION["login"]))
	{
		ShowLayout("Login");
	}
$clientlists=getClients();
$patientlists=getPatients();
$medicineslists=getMedicines();
	if(isset($_POST['tpl']) && isset($_POST['div']))
	{

		if(isset($_POST['val']))
		{
			global $db;

			$q="query";
			if(!isset($_POST['mth']))
			{
				//$q="select * from `".$forms->{$_POST['tpl']}->{'table'}."` where `".$forms->{$_POST['tpl']}->{'where'}."` = '".$_POST['val']."'";
				$q=$forms->{$_POST['tpl']}->{'sql2'}." where `".$forms->{$_POST['tpl']}->{'where'}."` = '".$_POST['val']."'";
				dp($q);
				$result = $db->query($q) or die($db->error);
				
			
			} else 
			{
				if($_POST['mth']=="new")
				{
					//print_r($_POST['val']);
					$q="insert into `".$forms->{$_POST['tpl']}->{'table'}. "` (";
					$k="";
					$v="";
					foreach ($_POST['val'] as $key => $value)
					{
						$k.= "`".$key."`, ";
						$v.="'".$value."', ";
					}
					$q.=substr($k,0,-2).") values (".substr($v,0,-2).")";	
					dp($q);
					$result = $db->query($q) or die($db->error);
					$q=$forms->{$_POST['tpl']}->{'sql2'}." where `".$forms->{$_POST['tpl']}->{'where'}."` = '".$db->insert_id."'";
					$result = $db->query($q) or die($db->error);
					$row = $result->fetch_array(MYSQLI_ASSOC);
					$_SESSION['content_data'] = $row;
					echo "<script>cp('".$forms->{$_POST['tpl']}->{'tpl'}."','contents','".$db->insert_id."');</script>";
				}
				if($_POST['mth']=="update")
				{
					dp($_POST['val']);
					$q="update `".$forms->{$_POST['tpl']}->{'table'}. "` set ";
					foreach ($_POST['val'] as $key => $value)
					{
						$q.= "`".$key."` = "."'".$value."', ";
					}
					$q=substr($q,0,-2)." where `".$forms->{$_POST['tpl']}->{'where'}."` ='".$_POST['val'][$forms->{$_POST['tpl']}->{'where'}]."'";	
					$result = $db->query($q) or die($db->error);
					$q=$forms->{$_POST['tpl']}->{'sql2'}." where `".$forms->{$_POST['tpl']}->{'where'}."` = '".$_POST['val'][$forms->{$_POST['tpl']}->{'where'}]."'";
					$result = $db->query($q) or die($db->error);
				}
			}
			dp($q);
			
				if ($db->error) {
				try {    
					throw new Exception("MySQL error $db->error <br> Query:<br> $q", $db->errno);    
				} catch(Exception $e ) {
					//echo "Error No: ".$e->getCode(). " - ". $e->getMessage() . "<br >";
					//echo nl2br($e->getTraceAsString());
				}

				};

			$row = $result->fetch_array(MYSQLI_ASSOC);
			$_SESSION['content_data'] = $row;
			dp($_SESSION['content_data']);
			if($_POST['tpl']=="CustomerOrderHDR_View")
			{
				if(!is_array($_POST['val'])){
				$_SESSION['nav']['orderid']=$_POST['val'];
				}else{
					$_SESSION['nav']['orderid']=$_POST['val']['OrderID'];
				}
				$_SESSION['nav']['ClientID']=$_SESSION['content_data']['ClientID'];
			}

			if($_POST['tpl']=="PrescriptionHDR_View")
			{
				if(!is_array($_POST['val'])){
			$_SESSION['nav']['prescriptionid']=$_POST['val'];
				}else{
					$_SESSION['nav']['prescriptionid']=$_POST['val']['PrescriptionID'];
				}
			}
			if($_POST['tpl']=="PrescriptionLine_View")
			{
			$_SESSION['nav']['prescriptionlineid']=$_POST['val'];
			}
			$_SESSION['nav']['medicalid']="null";			
			//extract($row, EXTR_PREFIX_ALL, $_POST['tpl']);
			//print_r(array_keys(get_defined_vars()));
			//echo "select * from ".$where->$_POST['tpl']->table." where ".$where->$_POST['tpl']->where." = ".$_POST['val'];
		}
		if($_POST['tpl']=="CustomerOrderHDR_List")
		{
			$_SESSION['nav']['orderid']="null";
			$_SESSION['nav']['ClientID']="null";
		}
		showTemplate($_POST['tpl'],$_POST['div']);
		
	}
	if(isset($_POST['username']) && isset($_POST['password']))
	{
		//echo "loging in";
		$username = $_POST['username'];//mysql_real_escape_string();
		$password = $_POST['password'];//mysql_real_escape_string();
		$result = $db->query("select 1 from employees where login = '$username' and password=MD5('$password') limit 1");

		if($result->num_rows)
		{
			$_SESSION['login'] = $username;
			$fingerprint             = md5($_SERVER['REMOTE_ADDR'].$_SERVER['HTTP_USER_AGENT']);
			$_SESSION['last_active'] = time();
			$_SESSION['fingerprint'] = $fingerprint;
	$_SESSION['nav']['orderid']="null";
	$_SESSION['nav']['ClientID']="null";
	$_SESSION['nav']['prescriptionid']="null";
	$_SESSION['nav']['prescriptionlineid']="null";
	$_SESSION['nav']['medicalid']="null";
			ShowLayout("Start");
		} else {
			$status = "Username or password wrong";
			ShowLayout("Login");
		}
		
	}

	if(isset($_GET['page']) && $_GET['page']=='Logout')
	{	
		ShowLayout("Logout");
	}
	if(isset($_GET['js']) )//&& (intval($_SERVER["REQUEST_TIME"]) - intval($_SESSION['js'])) < 99)
	{
			js();
	} else {
		//if(isset($_SESSION['js']))
		//echo (intval($_SERVER["REQUEST_TIME"]) - intval($_SESSION['js']));
	}
	if($_SERVER["REQUEST_URI"]!="/wecare/")
	{
		header("Location:/wecare/");
	}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html lang="en">
<head>
<title>WeCare Order System</title>
<link href="../image/favicon.ico" rel="icon" type="/image/x-icon" />
<link href="css/bootstrap_2.css" rel="stylesheet" media="screen">
<!--link href="css/bootstrap.min.css" rel="stylesheet"-->
<link href="css/signin.css" rel="stylesheet">
<link href="css/transparency.css" rel="stylesheet">
<link rel="stylesheet" href="css/smoothness/jquery-ui-1.10.3.custom.css" />

<script src="js/jquery.js"></script>
<script src="js/bootstrap.js"></script>
<script src="js/jquery-ui-1.10.3.custom.js"></script>
<script src="js/jquery.validation.js"></script>
<!--script src="js/jquery-ui-1.10.3.custom.min.js"></script-->

<script src="index.php?js=<?=$_SESSION['js']?>"></script>					
<?php echo '<script></script>';?>
</head>
<body style="padding-top: 0px;">
<div id="topbars"  class="navbar-static-top" role="navigation" ><?php echo $_SESSION["topbar"];?></div>
<div id="contents"><?php echo $_SESSION["content"];?></div>		
<div id="control1"></div>
	<!-- Client Modal -->
<div class="modal fade" id="Clients" tabindex="-1" role="dialog" aria-labelledby="ClientsLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="ClientsLabel">Please select customer</h4>
      </div>
      <div class="modal-body">
        <?=$clientlists;?>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
		<!-- OrderStatus Modal -->
	<div class="modal fade" id="OrderStatus" tabindex="-1" role="dialog" aria-labelledby="OrderStatusLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="OrderStatusLabel">Please select status</h4>
      </div>
      <div class="modal-body">
 
		  
		  <table class="table table-striped table-hover">
			  <tr onClick=getOrderStatus(1) style="cursor:pointer">
			<td>1</td><td>Processing</td>
			</tr>
			<tr onClick=getOrderStatus(2) style="cursor:pointer">
				<td>2</td><td>Completed</td>
		</tr>
		  </table>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
		<!-- ClientStatus Modal -->
	<div class="modal fade" id="ClientStatus" tabindex="-1" role="dialog" aria-labelledby="ClientStatusLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="ClientStatusLabel">Please select status</h4>
      </div>
      <div class="modal-body">
 
		  
		  <table class="table table-striped table-hover">
			  <tr onClick=getClientStatus(1) style="cursor:pointer">
			<td>1</td><td>Active</td>
			</tr>
			<tr onClick=getClientStatus(2) style="cursor:pointer">
				<td>2</td><td>Inactive</td>
		</tr>
		  </table>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
		<!-- Payment Modal -->
	<div class="modal fade" id="Payment" tabindex="-1" role="dialog" aria-labelledby="PaymentLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="PaymentLabel">Please select type</h4>
      </div>
      <div class="modal-body">
 
		  
		  <table class="table table-striped table-hover">
			  <tr onClick=getPayment(1) style="cursor:pointer">
			<td>1</td><td>COD</td>
			</tr>
			<tr onClick=getPayment(2) style="cursor:pointer">
				<td>2</td><td>Cheque</td>
		</tr>
		  </table>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->	
		<!-- Patient Modal -->
	<div class="modal fade" id="Patients" tabindex="-1" role="dialog" aria-labelledby="PatientLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="PatientLabel">Please select patient</h4>
      </div>
      <div class="modal-body">
  <?=$patientlists;?>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
		<!-- Medical Modal -->
	<div class="modal fade" id="Medicals" tabindex="-1" role="dialog" aria-labelledby="MedicalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="MedicalLabel">Please select medicines</h4>
      </div>
      <div class="modal-body">
  <?=$medicineslists;?>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->	
	

</body>

</html>