<?php
include("setting.php");
error_reporting(E_ALL);
    class Template
    {

    	protected $file;
		protected $values = array();
		
		public function __construct($file)
		{
			global $template_path,$template_files;
			$this->file = $template_path.$template_files->{$file}->{'path'};
			//echo $this->file." xxxfile";
		}
		
		public function set($key,$value)
		{
			$this->values[$key] = $value;
		}
		
		static public function merge($templates, $separator = "\n")
		{
			$output = "";
			
			foreach ($templates as $template)
			{
				$content = (get_class($template) !== "Template") 
				? "Expecting Template" 
				: $template->output();
				$output .= $content.$separator;
			}
			
			return $output;
		}
		
		public function output()
		{
			if(!file_exists($this->file))
			{
				return "Error loading template file ($this->file).";
			}
			ob_start();
			//$output = file_get_contents($this->file);
			global $UserManagementHDR_New_employeeid;
			include($this->file);
			$output = ob_get_clean();

			foreach ($this->values as $key => $value)
			{
				$tagToReplace = "[@$key]";
				$output = str_replace($tagToReplace, $value, $output);
			}

			preg_match ( '/(<body*.>)(.*)(<\/body>)/ims', $output, $matches );
			return $matches[2];
		}
    }


	//==============================================================================================
	function listdata($list)
		{
			global $db,$forms,$where;

			if($forms->$list!="")
			{
		
			$result = $db->query($forms->$list) or die ($db->error);
			$row = mysqli_fetch_assoc($result);
			$data = '		<table class="table table-striped table-hover">
	
				<thead>
					<tr>';
			foreach($row as $name=>$value)
			{
				$data.="<th>$name</th>";
			}
			$data.='				</tr>
				</thead>
				<tbody>
					';
			while($row)
			{
				$data.='<tr onClick=cp("'.$where->$list->tpl.'","contents","'.$row[$where->$list->column].'") style="cursor:pointer">';
				foreach($row as $value)
				{
					$data.="<td>$value</td>";
				}
				$data.='				</tr>
					';
				$row = mysqli_fetch_assoc($result);
			}
				$data.='			
				</tbody>
			</table>';
			return $data;
			}
	}
	//==============================================================================================
	function layout($file)
	{
		if($file!="Empty")
		{
		$layout = new Template($file);
		$layout->set($file,listdata($file));
		return $layout->output();
		}
	}
	//==============================================================================================
	function ShowLayout($page)
	{
		switch($page)
		{
			case "Logout":
			case "Login":
				$_SESSION['page'] = 'login';
				setLayout("",layout("Login"),"");
				break;
			case "Start":
				$_SESSION['page'] = "CustomerOrderHDR";
				setLayout("",layout("CustomerOrderHDR"),showMenu("Sales"));
				break;
				//echo "<script>$('#contents').html('".preg_replace( "/\r|\n/", "", $_SESSION['content'])."')</script>";
				echo "<script>$('#contents').attr('class','col-xs-12');</script>";
				echo '<script>$("#topbars").html("topbar")</script>';
				echo '<script>$("#sidebars").html("sidebar")</script>';
			default:
				exit();
		}	
	}
	function setLayout($t,$c,$s)
	{
		global $template_files;
		$_SESSION['topbar'] = $t;
		if($_SESSION['page']!='login')
		{
			$_SESSION['content'] = '<ol class="breadcrumb"><li>'.$template_files->{$_SESSION['page']}->{'status'}."</li></ol>".$_SESSION['page'].$c.'<script>cf();</script>';
		} else { 
			$_SESSION['content'] = $c;
		}
		$_SESSION['sidebar'] = $s;
	}
	function showTemplate($page,$div)
	{
		global $template_files;
		if($div == 'sidebars')
		{
			echo showMenu($page);
		} else if($page!="Empty")
		{
			$_SESSION['page'] = $page;
			//echo '<ol class="breadcrumb"><li>'.$template_files->{$_SESSION['page']}->{'status'}."</li></ol>".$_SESSION['page'].layout($page).'<script>cf();</script>';
			echo '<ol class="breadcrumb"><li>'.$template_files->{$_SESSION['page']}->{'status'}."</li></ol>".layout($page).'<script>cf();</script>';
		} else {
			//
		}

		exit();
	}
	function showMenu($page)
	{
		global $menus;	

			$menu ="<li class=\"dropdown\" style=\"cursor:pointer\"><a class=\"dropdown-toggle\" data-toggle=\"dropdown\">$page<b class=\"caret\"></b></a>";
			$menu.="<ul class=\"dropdown-menu\">";
			for($i=0;$i<count($menus->{$page});$i++)
			{
				$menu.="<li class=\"dropdown-header\">\\".$menus->{$page}[$i]->{"header"}."</li>";
				for($j=0;$j<count($menus->{$page}[$i]->{"items"});$j++)
				{
					$menu.="<li><a  onClick=".$menus->{$page}[$i]->{"items"}[$j]->{"command"}.";cc(this)>".$menus->{$page}[$i]->{"items"}[$j]->{"text"}."</a></li>\\\n";
				}
				if($i<count($menus->{$page})-1)
				{
				$menu.="<li class=\"divider\"></li>\\";
				}
			}
		
			$menu.="</ul>\\
            </li>";
		return $menu;

	}
	function js()
	{?>
	<?php
		global $timeout;
	header("Content-type: application/x-javascript");
	?> 
	t=null;
	$(document).ready(function()
	{
		cf();
		<?php
		if(isset($_SESSION['page']) && $_SESSION['page']!='login'){?>
		$("#contents").attr("class","col-xs-12");
		
		menu='<div class="navbar navbar-default navbar-static-top">\
			<div class="container">\
        	<div class="navbar-header">\
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">\
            <span class="icon-bar"></span>\
            <span class="icon-bar"></span>\
            <span class="icon-bar"></span>\
          </button>\
          <a class="navbar-brand" href="/wecare/"><img src="../image/WeCare.png" alt="Logo" style="max-width: 40%"></a>\
        </div>\
                <div class="navbar-collapse collapse">\
          <ul class="nav navbar-nav">';
          	menu+='<?PHP echo showMenu("Sales");?>'
          	menu+='<?PHP echo showMenu("Merchandise");?>'
          	menu+='<?PHP echo showMenu("Customer");?>'
          	menu+='<?PHP echo showMenu("Purchase");?>'
          	menu+='<?PHP echo showMenu("User");?>'
          	menu+='<?PHP echo showMenu("Report");?>'
          	menu+='</ul><ul class="nav navbar-nav navbar-right"><li class="active"><a href="index.php?logout">Logout</a></li></ul>\
        </div>\
      </div>\
   </div>'
 $("#topbars").html(menu);

/*		$("#topbars button:contains('Sales')").click(function(){cp("CustomerOrderHDR","contents");cp("Sales","sidebars")});
		$("#topbars button:contains('Merchandise')").click(function(){cp("MerchandiseHDR_List","contents");cp("Merchandise","sidebars")});
		$("#topbars button:contains('Customer')").click(function(){cp("CustomerClientHDR_List","contents");cp("Customer","sidebars")});
		$("#topbars button:contains('Purchase')").click(function(){cp("PurchaseHDRSupplier_List","contents");cp("Purchase","sidebars")});
		$("#topbars button:contains('User')").click(function(){cp("UserManagementHDR_List","contents");cp("User","sidebars")});		
		$("#topbars button:contains('Delivery')").click(function(){cp("DeliveryHDR_List","contents");cp("Delivery","sidebars")});
		$("#topbars button:contains('Report')").click(function(){cp("InventoryReport","contents");cp("Report","sidebars")});
		$("#topbars button:contains('Logout')").click(function(){location.href="index.php?logout"});*/
		<?php }else{?>
				$("#contents").attr("class","col-xs-12");<?php }?>
	})
	function cf()
	{ 
		$("form").append("<input type='hidden' name='page' value='<?=$_SESSION['page'];?>'>");
		$("form").attr("action","<?=$_SERVER['PHP_SELF']?>?submit");
		$("form").attr("method","post");
		$(":text[id*=Date]").datepicker().addClass("vDate");
		$("[placeholder='Login Name']").attr({"placeholder":"admin","name":"username"});
		$("[placeholder='Password']").attr({"placeholder":"admin","name":"password"});
	

		$("#control1").html("<script>clearTimeout(t);t=setTimeout(\"document.location='/wecare/'\",<?=$timeout*1100;?>)</script>");
		
		$('label.required').append('&nbsp;<strong>*</strong>&nbsp;');	
		$.validator.addMethod("cRequired", $.validator.methods.required, "<font color=red>Data required</font>");
		$.validator.addMethod("cDate", $.validator.methods.date,"<font color=red>Date required</font>");
		$.validator.addMethod("cNumber", $.validator.methods.number,"<font color=red>Number required</font>");
		$.validator.addMethod("cMinlength", $.validator.methods.minlength, $.format("<font color=red>at least {0} characters</font>"));
		
		$.validator.addClassRules("vText",{cRequired: true, cMinlength: 2});
		$.validator.addClassRules("vDate",{cRequired: true, cDate: true});
		$.validator.addClassRules("vNum", {cRequired: true, cNumber: true});
		$("form").removeAttr("novalidate");
		$('#searchCriteria').validate();
		
	}
	function cp(t,l,w)
	{
		$.post("index.php",
			{
				tpl:t,
				div:l,
				val:w
			},
			function(data,status)
			{
				$("#"+l).html(data);
				//$("#control1").html("<script>clearTimeout(t);t=setTimeout(\"document.location='/wecare/'\",<?=$timeout*1100;?>)</script>");
			});
	}
	function cc(o)
	{
		$(".dropdown").css("background-color","rgba(0,0,0,0)")
		$(o).parents(".dropdown").css("background-color","#ebebeb")
	}
	<?=exit();?>
	<?php 
	}


?>