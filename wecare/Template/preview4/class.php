<?php
include("setting.php");

    class Template
    {
    	protected $file;
		protected $values = array();
		
		public function __construct($file)
		{
			global $template_path,$template_files;
			$this->file = $template_path.$template_files->{$file};
			//echo $this->file." xxxfile";
		}
		
		public function set($key,$value)
		{
			$this->values[$key] = $value;
		}
		
		static public function merge($templates, $separator = "\n")
		{
			$output = "";
			
			foreach ($templates as $template)
			{
				$content = (get_class($template) !== "Template") 
				? "Expecting Template" 
				: $template->output();
				$output .= $content.$separator;
			}
			
			return $output;
		}
		
		public function output()
		{
			if(!file_exists($this->file))
			{
				return "Error loading template file ($this->file).";
			}
			
			$output = file_get_contents($this->file);
			

			foreach ($this->values as $key => $value)
			{
				$tagToReplace = "[@$key]";
				$output = str_replace($tagToReplace, $value, $html);
			}
			
			preg_match ( '/(<body*.>)(.*)(<\/body>)/ims', $output, $matches );
			return $matches[2];
		}
    }
	//==============================================================================================
	function layout($file)
	{
		if($file!="Empty")
		{
		$layout = new Template($file);
		//$layout ->set("content",$content_data);
		return $layout->output();
		}
	}
	//==============================================================================================
	function ShowLayout($page)
	{
		switch($page)
		{
			case "Logout":
			case "Login":
				$_SESSION['page'] = 'Login';
				setLayout("",layout("Login"),"");
				break;
			case "Start":
				$_SESSION['page'] = "Start";
				setLayout(layout("TopBar"),layout("Empty"),showMenu("Sales"));
				break;
				//echo "<script>$('#contents').html('".preg_replace( "/\r|\n/", "", $_SESSION['content'])."')</script>";
				echo "<script>$('#contents').attr('class','col-xs-12');</script>";
				echo '<script>$("#topbars").html("topbar")</script>';
				echo '<script>$("#sidebars").html("sidebar")</script>';
			default:
				exit();
		}	
	}
	function setLayout($t,$c,$s)
	{
		$_SESSION['topbar'] = $t;
		$_SESSION['content'] = $c;
		$_SESSION['sidebar'] = $s;
	}
	function showTemplate($page,$div)
	{
		if($div == 'sidebars')
		{
			echo showMenu($page);
		} else if($page!="Empty")
		{
			echo layout($page);
		} else {
			//echo $page;
		}
		//$_SESSION['page'] = $page;
		
		exit();
	}
	function showMenu($page)
	{
		global $menus;	
		$menu="<table align=\"right\">\n";
		for($i=0;$i<count($menus->{$page});$i++)
		{
		$menu.="<tr align=\"center\"><td class=\"nav-header\"><font size=\"3\">\n";
		$menu.=$menus->{$page}[$i]->{"header"};
		$menu.="</font></td></tr><tr align=\"center\"><td><div class=\"btn-group-vertical\">\n";
			for($j=0;$j<count($menus->{$page}[$i]->{"items"});$j++)
			{
				$menu.="<button type=\"button\" class=\"btn btn-default\" onClick=".$menus->{$page}[$i]->{"items"}[$j]->{"command"}.">".$menus->{$page}[$i]->{"items"}[$j]->{"text"}."</button>\n";
			}
		$menu.="</div></td></tr>";
		}	
		$menu.="</table>";
		return $menu;

	}
	function js()
	{?>
	<?php
	header("Content-type: application/x-javascript");
	?> 
	$(document).ready(function()
	{
		$("form").append("<input type='hidden' name='page' value='<?=$_SESSION['page'];?>'>");
		cf();
		<?php
		if(isset($_SESSION['page']) && $_SESSION['page']!='login'){?>
		$("#contents").attr("class","col-xs-10");
		$("#topbars button:contains('Sales')").click(function(){cp("CustomerOrderHDR","contents");cp("Sales","sidebars")});
		$("#topbars button:contains('Merchandise')").click(function(){cp("MerchandiseHDR_List","contents");cp("Merchandise","sidebars")});
		$("#topbars button:contains('Customer')").click(function(){cp("CustomerClientHDR_List","contents");cp("Customer","sidebars")});
		$("#topbars button:contains('Purchase')").click(function(){cp("Empty","contents");cp("Purchase","sidebars")});
		$("#topbars button:contains('User')").click(function(){cp("Empty","contents");cp("User","sidebars")});		
		$("#topbars button:contains('Delivery')").click(function(){cp("UserManagementHDR","contents");cp("Delivery","sidebars")});
		$("#topbars button:contains('Report')").click(function(){cp("UserManagementHDR","contents");cp("Report","sidebars")});
		$("#topbars button:contains('Logout')").click(function(){location.href="index.php?logout"});
		
		<?php }else{?>
				$("#contents").attr("class","col-xs-12");<?php }?>
	})
	function cf()
	{
		$("form").attr("action","<?=$_SERVER['PHP_SELF']?>?submit");
		$("form").attr("method","post");
		
		$("[placeholder='Login Name']").attr({"placeholder":"admin","name":"username"});
		$("[placeholder='Password']").attr({"placeholder":"admin","name":"password"});
	}
	function cp(t,l)
	{
		$.post("index.php",
			{
				tpl:t,
				div:l
			},
			function(data,status)
			{
				$("#"+l).html(data);
			});
	}
	<?=exit();?>
	<?php 
	}


?>