<?php
include("setting.php");

    class Template
    {
    	protected $file;
		protected $values = array();
		
		public function __construct($file)
		{
			global $template_path,$template_files;
			$this->file = $template_path.$template_files->{$file}->{'path'};
			//echo $this->file." xxxfile";
		}
		
		public function set($key,$value)
		{
			$this->values[$key] = $value;
		}
		
		static public function merge($templates, $separator = "\n")
		{
			$output = "";
			
			foreach ($templates as $template)
			{
				$content = (get_class($template) !== "Template") 
				? "Expecting Template" 
				: $template->output();
				$output .= $content.$separator;
			}
			
			return $output;
		}
		
		public function output()
		{
			if(!file_exists($this->file))
			{
				return "Error loading template file ($this->file).";
			}
			
			$output = file_get_contents($this->file);
			

			foreach ($this->values as $key => $value)
			{
				$tagToReplace = "[@$key]";
				$output = str_replace($tagToReplace, $value, $html);
			}
			
			preg_match ( '/(<body*.>)(.*)(<\/body>)/ims', $output, $matches );
			return $matches[2];
		}
    }
	//==============================================================================================
	function layout($file)
	{
		if($file!="Empty")
		{
		$layout = new Template($file);
		//$layout ->set("content",$content_data);
		return $layout->output();
		}
	}
	//==============================================================================================
	function ShowLayout($page)
	{
		switch($page)
		{
			case "Logout":
			case "Login":
				$_SESSION['page'] = 'login';
				setLayout("",layout("Login"),"");
				break;
			case "Start":
				$_SESSION['page'] = "CustomerOrderHDR";
				setLayout(layout("TopBar"),layout("CustomerOrderHDR"),showMenu("Sales"));
				break;
				//echo "<script>$('#contents').html('".preg_replace( "/\r|\n/", "", $_SESSION['content'])."')</script>";
				echo "<script>$('#contents').attr('class','col-xs-12');</script>";
				echo '<script>$("#topbars").html("topbar")</script>';
				echo '<script>$("#sidebars").html("sidebar")</script>';
			default:
				exit();
		}	
	}
	function setLayout($t,$c,$s)
	{
		global $template_files;
		$_SESSION['topbar'] = $t;
		if($_SESSION['page']!='login')
		{
			$_SESSION['content'] = '<ol class="breadcrumb"><li>'.$template_files->{$_SESSION['page']}->{'status'}."</li></ol>".$_SESSION['page'].$c.'<script>cf();</script>';
		} else { 
			$_SESSION['content'] = $c;
		}
		$_SESSION['sidebar'] = $s;
	}
	function showTemplate($page,$div)
	{
		global $template_files;
		if($div == 'sidebars')
		{
			echo showMenu($page);
		} else if($page!="Empty")
		{
			$_SESSION['page'] = $page;
			echo '<ol class="breadcrumb"><li>'.$template_files->{$_SESSION['page']}->{'status'}."</li></ol>".$_SESSION['page'].layout($page).'<script>cf();</script>';
		} else {
			//
		}

		exit();
	}
	function showMenu($page)
	{
		global $menus;	
		$menu="<table align=\"right\">\n";
		for($i=0;$i<count($menus->{$page});$i++)
		{
		$menu.="<tr align=\"center\"><td class=\"nav-header\"><font size=\"3\">\n";
		$menu.=$menus->{$page}[$i]->{"header"};
		$menu.="</font></td></tr><tr align=\"center\"><td><div class=\"btn-group-vertical\">\n";
			for($j=0;$j<count($menus->{$page}[$i]->{"items"});$j++)
			{
				$menu.="<button type=\"button\" class=\"btn btn-default\" onClick=".$menus->{$page}[$i]->{"items"}[$j]->{"command"}.">".$menus->{$page}[$i]->{"items"}[$j]->{"text"}."</button>\n";
			}
		$menu.="</div></td></tr>";
		}	
		$menu.="</table>";
		return $menu;

	}
	function js()
	{?>
	<?php
		global $timeout;
	header("Content-type: application/x-javascript");
	?> 
	t=null;
	$(document).ready(function()
	{
		cf();
		<?php
		if(isset($_SESSION['page']) && $_SESSION['page']!='login'){?>
		$("#contents").attr("class","col-xs-10");
		$("#topbars button:contains('Sales')").click(function(){cp("CustomerOrderHDR","contents");cp("Sales","sidebars")});
		$("#topbars button:contains('Merchandise')").click(function(){cp("MerchandiseHDR_List","contents");cp("Merchandise","sidebars")});
		$("#topbars button:contains('Customer')").click(function(){cp("CustomerClientHDR_List","contents");cp("Customer","sidebars")});
		$("#topbars button:contains('Purchase')").click(function(){cp("PurchaseHDRSupplier_List","contents");cp("Purchase","sidebars")});
		$("#topbars button:contains('User')").click(function(){cp("UserManagementHDR_List","contents");cp("User","sidebars")});		
		$("#topbars button:contains('Delivery')").click(function(){cp("DeliveryHDR_List","contents");cp("Delivery","sidebars")});
		$("#topbars button:contains('Report')").click(function(){cp("InventoryReport","contents");cp("Report","sidebars")});
		$("#topbars button:contains('Logout')").click(function(){location.href="index.php?logout"});
		<?php }else{?>
				$("#contents").attr("class","col-xs-12");<?php }?>
	})
	function cf()
	{ 
		$("form").append("<input type='hidden' name='page' value='<?=$_SESSION['page'];?>'>");
		$("form").attr("action","<?=$_SERVER['PHP_SELF']?>?submit");
		$("form").attr("method","post");
		$(":text[id*=Date]").datepicker().addClass("vDate");
		$("[placeholder='Login Name']").attr({"placeholder":"admin","name":"username"});
		$("[placeholder='Password']").attr({"placeholder":"admin","name":"password"});
	

		$("#control1").html("<script>clearTimeout(t);t=setTimeout(\"document.location='/wecare/'\",<?=$timeout*1100;?>)</script>");
		
		$('label.required').append('&nbsp;<strong>*</strong>&nbsp;');	
		$.validator.addMethod("cRequired", $.validator.methods.required, "<font color=red>Data required</font>");
		$.validator.addMethod("cDate", $.validator.methods.date,"<font color=red>Date required</font>");
		$.validator.addMethod("cNumber", $.validator.methods.number,"<font color=red>Number required</font>");
		$.validator.addMethod("cMinlength", $.validator.methods.minlength, $.format("<font color=red>at least {0} characters</font>"));
		
		$.validator.addClassRules("vText",{cRequired: true, cMinlength: 2});
		$.validator.addClassRules("vDate",{cRequired: true, cDate: true});
		$.validator.addClassRules("vNum", {cRequired: true, cNumber: true});
		$("form").removeAttr("novalidate");
		$('#searchCriteria').validate();
	}
	function cp(t,l)
	{
		$.post("index.php",
			{
				tpl:t,
				div:l
			},
			function(data,status)
			{
				$("#"+l).html(data);
				//$("#control1").html("<script>clearTimeout(t);t=setTimeout(\"document.location='/wecare/'\",<?=$timeout*1100;?>)</script>");
			});
	}

	<?=exit();?>
	<?php 
	}


?>