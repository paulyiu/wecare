-- phpMyAdmin SQL Dump
-- version 3.5.8.2
-- http://www.phpmyadmin.net
--
-- Host: sql102.byethost31.com
-- Generation Time: Dec 17, 2013 at 12:41 PM
-- Server version: 5.6.14-56
-- PHP Version: 5.3.3

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `b31_13867109_mydb`
--

-- --------------------------------------------------------

--
-- Table structure for table `clients`
--

DROP TABLE IF EXISTS `clients`;
CREATE TABLE IF NOT EXISTS `clients` (
  `ClientID` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Client ID (PK)',
  `ClientCompanyName` varchar(45) DEFAULT NULL COMMENT 'Client company name',
  `ClientTelephone` varchar(45) DEFAULT NULL COMMENT 'Client contact number',
  `ClientContactPerson` varchar(45) DEFAULT NULL COMMENT 'Client contact person',
  `ClientDiscount` int(11) DEFAULT NULL COMMENT 'Client discount rate',
  `ClientQuota` float DEFAULT NULL COMMENT 'Client quota limit',
  `ClientStatus` int(11) DEFAULT NULL COMMENT 'Client status',
  `Address` varchar(100) DEFAULT NULL,
  `DeliveryTeam` varchar(16) DEFAULT NULL COMMENT 'Delivery Team',
  PRIMARY KEY (`ClientID`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COMMENT='Client record, ClientID is the primary key' AUTO_INCREMENT=8 ;

-- --------------------------------------------------------

--
-- Table structure for table `deliveryhdr`
--

DROP TABLE IF EXISTS `deliveryhdr`;
CREATE TABLE IF NOT EXISTS `deliveryhdr` (
  `DeliveryID` int(11) NOT NULL DEFAULT '0' COMMENT 'Delivery ID (PK)',
  `OrderID` int(11) NOT NULL COMMENT 'Order ID (FK)',
  `ClientID` int(11) NOT NULL COMMENT 'Client ID (FK)',
  `LocationID` int(11) NOT NULL COMMENT 'Location ID (FK)',
  `EmployeeID` int(11) NOT NULL COMMENT 'Employee ID (FK)',
  `DeliveryCreateTime` datetime DEFAULT NULL COMMENT 'Create time',
  `DeliveryLastEditTime` datetime DEFAULT NULL COMMENT 'Last modify time',
  `DeliveryExpectDate` datetime DEFAULT NULL COMMENT 'Exprect delivery time',
  `DeliveryTotalPrice` float DEFAULT NULL COMMENT 'Delivery total price',
  PRIMARY KEY (`DeliveryID`),
  KEY `fk_deliveryhdr_order1_idx` (`OrderID`),
  KEY `fk_deliveryhdr_clients1_idx` (`ClientID`),
  KEY `fk_deliveryhdr_locations1_idx` (`LocationID`),
  KEY `fk_deliveryhdr_employees1_idx` (`EmployeeID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='Delivery note header, DeliveryID is the primary key, OrderID is foreign key linked to Orders table, ClientID is foreign key linked to Clients table, LocationID is foreign key linked to Locations table, DeliveryEmployeeID is foreign key linked to Employees table';

-- --------------------------------------------------------

--
-- Table structure for table `deliveryline`
--

DROP TABLE IF EXISTS `deliveryline`;
CREATE TABLE IF NOT EXISTS `deliveryline` (
  `DeliveryLineID` int(11) NOT NULL COMMENT 'Delivery ID (PK)',
  `DeliveryID` int(11) NOT NULL COMMENT 'Delivery ID (FK)',
  `MedicalID` int(11) NOT NULL COMMENT 'Medical ID (FK)',
  `LotID` int(11) NOT NULL COMMENT 'Lot ID (FK)',
  `DeliveryLineQty` int(11) DEFAULT NULL COMMENT 'Quantity',
  `DeliveryLineItemPrice` float DEFAULT NULL COMMENT 'Line item amount',
  PRIMARY KEY (`DeliveryLineID`),
  KEY `fk_deliveryline_deliveryhdr1_idx` (`DeliveryID`),
  KEY `fk_deliveryline_medical1_idx` (`MedicalID`),
  KEY `fk_deliveryline_lots1_idx` (`LotID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='Delivery detail, DeliveryLineID is the primary key of DeliveryLine, Medicines is foreign key linked to Medicines table';

-- --------------------------------------------------------

--
-- Table structure for table `employees`
--

DROP TABLE IF EXISTS `employees`;
CREATE TABLE IF NOT EXISTS `employees` (
  `EmployeeID` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Employee ID (PK)',
  `EmployeeFirstName` varchar(45) DEFAULT NULL COMMENT 'Employee first name',
  `EmployeeLastName` varchar(45) DEFAULT NULL COMMENT 'Employee last name',
  `EmployeeTitle` varchar(45) DEFAULT NULL COMMENT 'Employee Title',
  `Login` varchar(45) DEFAULT NULL COMMENT 'Login name',
  `Password` varchar(45) DEFAULT NULL COMMENT 'Password',
  `EmployeeStatus` int(11) DEFAULT NULL COMMENT 'Employee status',
  `Gender` int(1) NOT NULL,
  `Phone` varchar(8) NOT NULL,
  `Address` varchar(100) NOT NULL,
  `Email` varchar(50) NOT NULL,
  `Salary` int(9) NOT NULL,
  `DateOfBirth` date NOT NULL,
  PRIMARY KEY (`EmployeeID`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COMMENT='Employees record, EmployeeID is the primary key of Employees table' AUTO_INCREMENT=4 ;

-- --------------------------------------------------------

--
-- Table structure for table `locations`
--

DROP TABLE IF EXISTS `locations`;
CREATE TABLE IF NOT EXISTS `locations` (
  `LocationID` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Location ID (PK)',
  `ClientID` int(11) NOT NULL COMMENT 'Client ID (FK)',
  `LocationFacilityName` varchar(45) DEFAULT NULL COMMENT 'Location description',
  `LocationFloor` varchar(45) DEFAULT NULL COMMENT 'Location floor',
  `LocationWingNo` varchar(45) DEFAULT NULL COMMENT 'Location wing number',
  `LocationBuilding` varchar(255) DEFAULT NULL COMMENT 'Location building',
  `LocationRoad` varchar(45) DEFAULT NULL COMMENT 'Location road',
  `LocationCountry` varchar(45) DEFAULT NULL COMMENT 'Location country',
  `LocationStatus` int(11) DEFAULT NULL COMMENT 'Location status',
  PRIMARY KEY (`LocationID`),
  KEY `fk_locations_clients1_idx` (`ClientID`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COMMENT='Location detail for clients, LocationID is the primary key of Locations table, ClientID is foreign key linked to Clients table' AUTO_INCREMENT=4 ;

-- --------------------------------------------------------

--
-- Table structure for table `lots`
--

DROP TABLE IF EXISTS `lots`;
CREATE TABLE IF NOT EXISTS `lots` (
  `LotID` int(11) NOT NULL COMMENT 'Lot ID (PK)',
  `MedicalID` int(11) NOT NULL COMMENT 'Medical ID (FK)',
  `LotsQty` int(11) DEFAULT NULL COMMENT 'Quantity',
  `LotsExpireDate` date DEFAULT NULL COMMENT 'Expire Date',
  PRIMARY KEY (`LotID`),
  KEY `fk_lots_medical1_idx` (`MedicalID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='Lot record for medicines, LotID is the primary key of Lots table, MedicineID is foreign key linked to Medicines table';

-- --------------------------------------------------------

--
-- Table structure for table `medical`
--

DROP TABLE IF EXISTS `medical`;
CREATE TABLE IF NOT EXISTS `medical` (
  `MedicalID` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Medical ID (PK)',
  `MedicalName` varchar(45) DEFAULT NULL COMMENT 'Medical name',
  `MedicalDescription` varchar(45) DEFAULT NULL COMMENT 'Medical description',
  `MedicalPrice` float DEFAULT NULL COMMENT 'Medical price',
  `MedicalRetailPrice` float DEFAULT NULL COMMENT 'Medicine retail price',
  `MedicalStatus` int(11) DEFAULT NULL COMMENT 'Medicine status',
  `MedicalTotalQty` int(11) DEFAULT NULL COMMENT 'Medical total quantity',
  `MedicalType` varchar(45) DEFAULT NULL COMMENT 'Drag type',
  PRIMARY KEY (`MedicalID`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COMMENT='Item details record, MedicineID is the primary key of Item table' AUTO_INCREMENT=10 ;

-- --------------------------------------------------------

--
-- Table structure for table `order`
--

DROP TABLE IF EXISTS `order`;
CREATE TABLE IF NOT EXISTS `order` (
  `OrderID` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Order ID (PK)',
  `EmployeeID` int(11) NOT NULL COMMENT 'Employee ID (FK)',
  `ClientID` int(11) NOT NULL COMMENT 'Client ID (FK)',
  `LocationID` int(11) NOT NULL COMMENT 'Location ID (FK)',
  `PaymentID` int(11) DEFAULT NULL COMMENT 'Order payment ID (FK)',
  `OrderPackingEmpID` int(11) DEFAULT NULL COMMENT 'Employee ID for packing (FK)',
  `OrderFinalCheckEmpID` int(11) DEFAULT NULL COMMENT 'Employee ID for final check (FK)',
  `OrderCreateTime` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Create time',
  `OrderLastEditTime` date DEFAULT NULL COMMENT 'Last modify time',
  `OrderExpectDeliveryDate` date DEFAULT NULL COMMENT 'Expect delivery time',
  `OrderStatus` int(11) DEFAULT NULL COMMENT 'Order status (0: cancel; 1:complete; 2:inactive)',
  `OrderTotalPrice` float DEFAULT NULL COMMENT 'Order tatal price',
  PRIMARY KEY (`OrderID`),
  KEY `fk_order_employees1_idx` (`EmployeeID`),
  KEY `fk_order_clients1_idx` (`ClientID`),
  KEY `fk_order_locations1_idx` (`LocationID`),
  KEY `fk_order_payment1_idx` (`PaymentID`),
  KEY `fk_order_employees2_idx` (`OrderPackingEmpID`),
  KEY `fk_order_employees3_idx` (`OrderFinalCheckEmpID`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COMMENT='Order details, OrderID is the primary key of OrderHdr table, ClientID is foreign key linked to Clients table, LocationID is foreign key linked to Locations table, EmployeeID is foreign key linked to Employees table' AUTO_INCREMENT=102 ;

-- --------------------------------------------------------

--
-- Table structure for table `patient`
--

DROP TABLE IF EXISTS `patient`;
CREATE TABLE IF NOT EXISTS `patient` (
  `PatientID` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Patient ID (PK)',
  `ClientID` int(11) NOT NULL COMMENT 'Client ID (FK)',
  `LocationID` int(11) NOT NULL COMMENT 'Patient location (FK)',
  `PatientFirstName` varchar(45) DEFAULT NULL COMMENT 'Patient first name',
  `PatientLastName` varchar(45) DEFAULT NULL COMMENT 'Patient last name',
  `PatientGender` int(11) DEFAULT NULL COMMENT 'Patient gender',
  `PatientDateOfBirth` date DEFAULT NULL COMMENT 'Patient date of birth',
  `PatientRemark` varchar(45) DEFAULT NULL COMMENT 'Patient remark',
  `PatientContact` varchar(45) DEFAULT NULL COMMENT 'Patient contact',
  `PatientRoom` varchar(45) DEFAULT NULL COMMENT 'Patient room number',
  `PatientAddress` varchar(45) DEFAULT NULL COMMENT 'Patient address',
  `PatientStatus` int(11) DEFAULT NULL COMMENT 'Patient status',
  PRIMARY KEY (`PatientID`),
  KEY `fk_patient_clients1_idx` (`ClientID`),
  KEY `fk_patient_locations1_idx` (`LocationID`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COMMENT='Keep patient record, patientID is the primary key of Patient table, clientID is foreign key linked to Clients table, LocationID is foreign key linked to Locations table' AUTO_INCREMENT=4 ;

-- --------------------------------------------------------

--
-- Table structure for table `payment`
--

DROP TABLE IF EXISTS `payment`;
CREATE TABLE IF NOT EXISTS `payment` (
  `PaymentID` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Payment ID (PK)',
  `ClientID` int(11) NOT NULL COMMENT 'Client ID (FK)',
  `EmployeeID` int(11) NOT NULL COMMENT 'EmployeeID (FK)',
  `PrescriptionID` int(11) NOT NULL COMMENT 'Prescription ID (FK)',
  `PaymentCreateTime` datetime DEFAULT CURRENT_TIMESTAMP COMMENT 'Payment create time',
  `PaymentLastEditTime` datetime DEFAULT NULL COMMENT 'Payment last modify time',
  `PaymentType` int(11) DEFAULT NULL COMMENT 'Payment type',
  `PaymentAmount` float DEFAULT NULL COMMENT 'Payment amount',
  `OrderID` int(11) NOT NULL,
  PRIMARY KEY (`PaymentID`),
  KEY `fk_payment_clients1_idx` (`ClientID`),
  KEY `fk_payment_employees1_idx` (`EmployeeID`),
  KEY `fk_payment_prescription1_idx` (`PrescriptionID`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COMMENT='Keep payment transaction record, PaymentID is the primary key, ClientID is foreign key linked to Clients table, EmployeeID is foreign key linked to Employees table' AUTO_INCREMENT=12 ;

-- --------------------------------------------------------

--
-- Table structure for table `pohdr`
--

DROP TABLE IF EXISTS `pohdr`;
CREATE TABLE IF NOT EXISTS `pohdr` (
  `POID` int(11) NOT NULL COMMENT 'PO ID (PK)',
  `SupplierID` int(11) NOT NULL COMMENT 'Supplier ID (FK)',
  `EmployeeID` int(11) NOT NULL COMMENT 'Employee ID (FK)',
  `POCreateTime` datetime DEFAULT NULL COMMENT 'Create Time',
  `POLastEditTime` datetime DEFAULT NULL COMMENT 'Last modify time',
  `POExpectDelivery` datetime DEFAULT NULL COMMENT 'Expected delivery time',
  `POTotal` float DEFAULT NULL COMMENT 'Total amount of the PO',
  `POStatus` int(11) DEFAULT NULL COMMENT 'PO status',
  PRIMARY KEY (`POID`),
  KEY `fk_pohdr_suppliers1_idx` (`SupplierID`),
  KEY `fk_pohdr_employees1_idx` (`EmployeeID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='Purchase order header record, POHDR_ID is the primary key of POhdr table, SupplierID is foreign key linked to Suppliers table, EmployeeID is foreign key linked to Employees table';

-- --------------------------------------------------------

--
-- Table structure for table `poline`
--

DROP TABLE IF EXISTS `poline`;
CREATE TABLE IF NOT EXISTS `poline` (
  `POLineID` int(11) NOT NULL COMMENT 'PO Line ID (PK)',
  `POID` int(11) NOT NULL COMMENT 'PO ID (FK)',
  `MedicalID` int(11) NOT NULL COMMENT 'PO item id (FK)',
  `POLinePrice` float DEFAULT NULL COMMENT 'Purchase line price',
  `POLineQty` int(11) DEFAULT NULL COMMENT 'Purchase line quility',
  PRIMARY KEY (`POLineID`),
  KEY `fk_poline_pohdr1_idx` (`POID`),
  KEY `fk_poline_medical1_idx` (`MedicalID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='Purchase order detail record, LineID is the primary key of POLine table, POID is foreign key linked to POHdr table';

-- --------------------------------------------------------

--
-- Table structure for table `prescription`
--

DROP TABLE IF EXISTS `prescription`;
CREATE TABLE IF NOT EXISTS `prescription` (
  `PrescriptionID` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Prescription ID (PK)',
  `OrderID` int(11) NOT NULL COMMENT 'Order ID (FK)',
  `PatientID` int(11) NOT NULL COMMENT 'Patient ID (FK)',
  PRIMARY KEY (`PrescriptionID`),
  KEY `fk_prescription_order1_idx` (`OrderID`),
  KEY `fk_prescription_patient1_idx` (`PatientID`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COMMENT='Prescriptionrecord, PrescriptionID is the primary key of Prescription table, OrderID is foreign key linked to Order table, PatientID is foreign key linked to Patient table' AUTO_INCREMENT=51 ;

-- --------------------------------------------------------

--
-- Table structure for table `prescriptionline`
--

DROP TABLE IF EXISTS `prescriptionline`;
CREATE TABLE IF NOT EXISTS `prescriptionline` (
  `PrescriptionLineID` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Prescription line ID (PK)',
  `PrescriptionID` int(11) NOT NULL COMMENT 'Prescription ID (FK)',
  `MedicalID` int(11) NOT NULL COMMENT 'Medical ID (FK)',
  `PrescriptionLineQty` int(11) DEFAULT NULL COMMENT 'Quantity',
  `PrescriptionLineMethod` varchar(45) DEFAULT NULL COMMENT 'Prescription Method',
  `LotID` int(11) NOT NULL,
  PRIMARY KEY (`PrescriptionLineID`),
  KEY `fk_prescriptionline_prescription1_idx` (`PrescriptionID`),
  KEY `fk_prescriptionline_medical1_idx` (`MedicalID`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COMMENT='Prescription line detail, PrescriptionLineID is the primary key of PrescriptionLine, PrescriptionID is foreign key to Prescription, MedicalID is foreign key linked to Medicines table' AUTO_INCREMENT=49 ;

-- --------------------------------------------------------

--
-- Table structure for table `receivehdr`
--

DROP TABLE IF EXISTS `receivehdr`;
CREATE TABLE IF NOT EXISTS `receivehdr` (
  `ReceiveHDRID` int(11) NOT NULL COMMENT 'Receive ID (PK)',
  `POID` int(11) NOT NULL COMMENT 'Purchase order ID (FK)',
  `EmployeeID` int(11) NOT NULL COMMENT 'Employee ID (FK)',
  `SupplierID` int(11) NOT NULL COMMENT 'Supplier ID (FK)',
  `ReceiveCreateTime` datetime DEFAULT NULL COMMENT 'Create time',
  `ReceiveLastEditTime` datetime DEFAULT NULL COMMENT 'Last modify time',
  `ReceiveTotalAmount` float DEFAULT NULL COMMENT 'Receive total amount',
  PRIMARY KEY (`ReceiveHDRID`),
  KEY `fk_receivehdr_employees1_idx` (`EmployeeID`),
  KEY `fk_receivehdr_suppliers1_idx` (`SupplierID`),
  KEY `fk_receivehdr_pohdr1_idx` (`POID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='Receive header that link information of order receive together, ReceiveID is the primary key of ReceiveHDR, POID is foreign key linked to PO';

-- --------------------------------------------------------

--
-- Table structure for table `receiveline`
--

DROP TABLE IF EXISTS `receiveline`;
CREATE TABLE IF NOT EXISTS `receiveline` (
  `ReceiveLineID` int(11) NOT NULL COMMENT 'Receive line ID (PK)',
  `medical_MedicalID` int(11) NOT NULL COMMENT 'Receive medical ID (FK)',
  `receivehdr_ReceiveHDRID` int(11) NOT NULL COMMENT 'Receive header ID (FK)',
  `ReceiveLinePrice` float DEFAULT NULL COMMENT 'Delivery note total price',
  `ReceiveLineQty` int(11) DEFAULT NULL COMMENT 'Receive line quitily',
  `ReceiveLotNo` int(11) DEFAULT NULL COMMENT 'Lot Number of receive line',
  PRIMARY KEY (`ReceiveLineID`),
  KEY `fk_receiveline_medical1_idx` (`medical_MedicalID`),
  KEY `fk_receiveline_receivehdr1_idx` (`receivehdr_ReceiveHDRID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='Delivery detail records and total price of receive line, ReceiveLineID is the primary key of ReceiveLine, LineID is foreign key linked to POLine table';

-- --------------------------------------------------------

--
-- Table structure for table `role`
--

DROP TABLE IF EXISTS `role`;
CREATE TABLE IF NOT EXISTS `role` (
  `RoleID` int(11) NOT NULL COMMENT 'Role ID (PK)',
  `EmployeeID` int(11) NOT NULL COMMENT 'Employee ID (FK)',
  `RoleName` varchar(45) DEFAULT NULL COMMENT 'Role name',
  `RoleDescription` varchar(45) DEFAULT NULL COMMENT 'Role description',
  PRIMARY KEY (`RoleID`),
  KEY `fk_role_employees_idx` (`EmployeeID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='For assign permission and restrict access to the system. RoleID is primary key of Role table, EmployeeID is foreign key linked to Employees table.';

-- --------------------------------------------------------

--
-- Table structure for table `suppliers`
--

DROP TABLE IF EXISTS `suppliers`;
CREATE TABLE IF NOT EXISTS `suppliers` (
  `SupplierID` int(11) NOT NULL COMMENT 'Supplier ID (PK)',
  `SupplierContactPerson` varchar(45) DEFAULT NULL COMMENT 'Supplier contact person',
  `SupplierTel` varchar(45) DEFAULT NULL COMMENT 'Supplier telephone',
  `SupplierAddress` varchar(45) DEFAULT NULL COMMENT 'Supplier address',
  `SupplierStatus` varchar(45) DEFAULT NULL COMMENT 'Supplier status',
  `SupplierName` varchar(64) NOT NULL,
  PRIMARY KEY (`SupplierID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='Keep supplier record and contact detail, supplierID is the primary key of Suppliers';

-- --------------------------------------------------------

--
-- Table structure for table `test2`
--

DROP TABLE IF EXISTS `test2`;
CREATE TABLE IF NOT EXISTS `test2` (
  `t` int(11) NOT NULL,
  `e` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COMMENT='test';

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
