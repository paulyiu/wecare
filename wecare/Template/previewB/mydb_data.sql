-- phpMyAdmin SQL Dump
-- version 3.5.8.2
-- http://www.phpmyadmin.net
--
-- Host: sql102.byethost31.com
-- Generation Time: Dec 17, 2013 at 12:42 PM
-- Server version: 5.6.14-56
-- PHP Version: 5.3.3

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `b31_13867109_mydb`
--

--
-- Dumping data for table `clients`
--

INSERT INTO `clients` (`ClientID`, `ClientCompanyName`, `ClientTelephone`, `ClientContactPerson`, `ClientDiscount`, `ClientQuota`, `ClientStatus`, `Address`, `DeliveryTeam`) VALUES
(1, 'Queen Mary Hospital', '29657563', 'Otto', 2, 200, 1, 'Pok Fu Lam, Hong Kong Island, Hong Kong', 'Team A'),
(2, 'Tung Wah Eastern Hospital', '23687452', 'Jacob', 5, 2000, 1, 'So Kon Po, Causeway Bay, Hong Kong', 'Team A'),
(3, 'Queen Elizabeth Hospital', '27863142', 'Larry', 10, 4000, 0, 'King''s Park,, Kowloon, Hong Kong', 'Team B'),
(4, 'Polly', '32432423', 'Polly', 5, 500, 2, 'king''s road', 'Team A'),
(5, 'Peter', '234234', 'Peter', 6, 5002, 1, 'test address 123', 'Team A'),
(6, 'Nam Long Hospital', '29030000', 'Kenny', 0, 0, 1, '30 Nam Long Shan Rd, Wong Chuk Hang', 'Team B'),
(7, 'Tuen Mun Hospital', '24685111', 'Jason', 0, 0, 1, 'Address, 23 Tsing Chung Koon Road, Tuen Mun, NT.', 'Team B');

--
-- Dumping data for table `employees`
--

INSERT INTO `employees` (`EmployeeID`, `EmployeeFirstName`, `EmployeeLastName`, `EmployeeTitle`, `Login`, `Password`, `EmployeeStatus`, `Gender`, `Phone`, `Address`, `Email`, `Salary`, `DateOfBirth`) VALUES
(1, 'Admin', 'Super', 'Site admin', 'admin', '21232f297a57a5a743894a0e4a801fc3', 1, 1, '88885555', 'HK', 'tom@gmail.com', 110001, '0000-00-00'),
(2, NULL, NULL, 'sdf', 'Paul', NULL, 23, 1, '324234', 'safsdf', 'asf@adf.c', 234234, '0000-00-00');

--
-- Dumping data for table `locations`
--

INSERT INTO `locations` (`LocationID`, `ClientID`, `LocationFacilityName`, `LocationFloor`, `LocationWingNo`, `LocationBuilding`, `LocationRoad`, `LocationCountry`, `LocationStatus`) VALUES
(1, 1, 'Union Hospital', '', NULL, 'LG/F,logistic Department,1 union road, kwun tong', '', NULL, NULL),
(2, 2, 'Tseng Kwun O Hospital', NULL, NULL, '2/F,Logistic Department, 2 choi ming st,tseng kwun O', NULL, NULL, NULL),
(3, 3, 'Elisablish Hosptial', NULL, NULL, '1/F, 123 golf road, jordan', NULL, NULL, NULL);

--
-- Dumping data for table `lots`
--

INSERT INTO `lots` (`LotID`, `MedicalID`, `LotsQty`, `LotsExpireDate`) VALUES
(120140530, 1, 20, '2013-12-03'),
(120140917, 1, 70, '2013-12-04'),
(120140918, 2, 70, '2013-12-04'),
(120140919, 2, 70, '2013-12-04'),
(120140920, 3, 70, '2013-12-04'),
(120140921, 3, 70, '2013-12-04'),
(120140922, 4, 70, '2013-12-04'),
(120140923, 4, 70, '2013-12-04'),
(120140924, 5, 70, '2013-12-04'),
(120140925, 5, 70, '2013-12-04'),
(120140926, 6, 70, '2013-12-04'),
(120140927, 6, 70, '2013-12-04'),
(120140928, 7, 70, '2013-12-04'),
(120140929, 7, 70, '2013-12-04');

--
-- Dumping data for table `medical`
--

INSERT INTO `medical` (`MedicalID`, `MedicalName`, `MedicalDescription`, `MedicalPrice`, `MedicalRetailPrice`, `MedicalStatus`, `MedicalTotalQty`, `MedicalType`) VALUES
(1, 'Robitussin', 'Cough up', 15, 20, 1, 200, '7569841'),
(2, 'Tylenol', 'Cold', 18, 25, 1, 50, '7569841'),
(3, 'Sudafed', 'Sensitive', 19, 45, 0, 50, '7569841'),
(4, 'Panadol', 'Panadol', 10, 20, 1, 50, '2378652'),
(5, 'Anti-cancer drugs', 'Anti-cancer drugs', 50, 80, 1, 150, '2378652'),
(6, 'Childrens Motrin', 'Children fever', 12, 25, 1, 600, '7569841'),
(7, 'Day Quil', 'Ease pain', 10, 20, 1, 600, '7569841'),
(8, 'Aspirin', 'Aspirin', 23, 43, 1, 343, NULL),
(9, 'Glucobay', 'Glucobay', 34, 56, 1, 234, NULL);

--
-- Dumping data for table `order`
--

INSERT INTO `order` (`OrderID`, `EmployeeID`, `ClientID`, `LocationID`, `PaymentID`, `OrderPackingEmpID`, `OrderFinalCheckEmpID`, `OrderCreateTime`, `OrderLastEditTime`, `OrderExpectDeliveryDate`, `OrderStatus`, `OrderTotalPrice`) VALUES
(101, 0, 7, 0, NULL, NULL, NULL, '2013-12-17 17:39:52', NULL, NULL, NULL, NULL),
(100, 0, 6, 0, NULL, NULL, NULL, '2013-12-17 17:37:25', NULL, NULL, NULL, NULL),
(99, 0, 6, 0, NULL, NULL, NULL, '2013-12-17 17:23:44', NULL, NULL, NULL, NULL),
(98, 0, 7, 0, NULL, NULL, NULL, '2013-12-17 17:21:25', NULL, NULL, 1, NULL),
(97, 0, 6, 0, NULL, NULL, NULL, '2013-12-17 14:30:35', NULL, NULL, NULL, NULL),
(96, 0, 1, 0, NULL, NULL, NULL, '2013-12-16 19:28:00', NULL, NULL, NULL, NULL),
(95, 0, 2, 0, NULL, NULL, NULL, '2013-12-16 19:25:15', NULL, NULL, 2, NULL),
(94, 0, 3, 0, NULL, NULL, NULL, '2013-12-16 19:23:38', NULL, NULL, 1, NULL),
(93, 0, 3, 0, NULL, NULL, NULL, '2013-12-16 19:16:58', NULL, NULL, NULL, NULL),
(92, 0, 4, 0, NULL, NULL, NULL, '2013-12-16 19:16:02', NULL, NULL, 1, NULL),
(91, 0, 1, 0, NULL, NULL, NULL, '2013-12-16 19:13:40', NULL, NULL, 1, NULL),
(90, 0, 2, 0, NULL, NULL, NULL, '2013-12-16 16:51:37', NULL, NULL, NULL, NULL),
(89, 0, 5, 0, NULL, NULL, NULL, '2013-12-16 16:48:25', NULL, NULL, 1, NULL),
(88, 0, 4, 0, NULL, NULL, NULL, '2013-12-16 11:57:58', NULL, NULL, NULL, NULL),
(87, 0, 3, 0, NULL, NULL, NULL, '2013-12-15 20:44:30', NULL, NULL, 2, NULL),
(86, 0, 1, 0, NULL, NULL, NULL, '2013-12-15 18:48:03', NULL, NULL, 1, NULL),
(85, 0, 1, 0, NULL, NULL, NULL, '2013-12-15 18:08:27', NULL, NULL, NULL, NULL),
(82, 0, 1, 0, NULL, NULL, NULL, '2013-12-15 15:20:48', NULL, NULL, 1, NULL),
(83, 0, 1, 0, NULL, NULL, NULL, '2013-12-15 17:48:25', NULL, NULL, 1, NULL),
(84, 0, 4, 0, NULL, NULL, NULL, '2013-12-15 17:49:46', NULL, NULL, 2, NULL);

--
-- Dumping data for table `patient`
--

INSERT INTO `patient` (`PatientID`, `ClientID`, `LocationID`, `PatientFirstName`, `PatientLastName`, `PatientGender`, `PatientDateOfBirth`, `PatientRemark`, `PatientContact`, `PatientRoom`, `PatientAddress`, `PatientStatus`) VALUES
(1, 1, 1, 'Veronica', 'Fung', 0, '2013-11-05', 'xxx', '77778888', '4401', 'HK', 1),
(2, 2, 2, 'Raymond', 'Chan', 1, '2013-11-13', 'ccc', '77779999', '3403', 'KL', 0),
(3, 3, 3, 'Rita', 'Tse', 0, '2013-11-13', NULL, '77779999', '1011', 'KL', 1);

--
-- Dumping data for table `payment`
--

INSERT INTO `payment` (`PaymentID`, `ClientID`, `EmployeeID`, `PrescriptionID`, `PaymentCreateTime`, `PaymentLastEditTime`, `PaymentType`, `PaymentAmount`, `OrderID`) VALUES
(1, 1, 0, 47, '2013-11-05 00:00:00', '2013-11-20 00:00:00', 1, 250, 47),
(2, 2, 0, 48, '2013-11-05 00:00:00', '2013-11-05 00:00:00', 1, 250, 48),
(3, 3, 0, 49, '2013-11-05 00:00:00', '2013-11-05 00:00:00', 2, 800, 49),
(4, 3, 0, 0, '2013-12-16 00:00:00', NULL, 2, 1001, 82),
(5, 3, 0, 0, '2013-12-16 11:54:31', NULL, 1, 1212, 82),
(6, 2, 0, 0, '2013-12-16 12:06:43', NULL, 1, 2001, 90),
(7, 5, 0, 0, '2013-12-16 12:30:12', NULL, 2, 123, 89),
(8, 1, 0, 0, '2013-12-16 14:06:56', NULL, 2, 123, 82),
(9, 3, 0, 0, '2013-12-16 14:17:54', NULL, 1, 112, 93),
(10, 1, 0, 0, '2013-12-16 14:29:29', NULL, 2, 2333, 96),
(11, 4, 0, 0, '2013-12-17 08:10:53', NULL, 2, 500, 88);

--
-- Dumping data for table `pohdr`
--

INSERT INTO `pohdr` (`POID`, `SupplierID`, `EmployeeID`, `POCreateTime`, `POLastEditTime`, `POExpectDelivery`, `POTotal`, `POStatus`) VALUES
(1, 1, 0, '2013-11-04 00:00:00', '2013-11-05 00:00:00', '2013-11-18 00:00:00', 20, 2),
(2, 2, 0, '2013-11-04 00:00:00', '2013-11-05 00:00:00', '2013-11-19 00:00:00', 22, 2),
(3, 3, 0, '2013-11-05 00:00:00', '2013-11-06 00:00:00', '2013-11-19 00:00:00', 25, 1);

--
-- Dumping data for table `poline`
--

INSERT INTO `poline` (`POLineID`, `POID`, `MedicalID`, `POLinePrice`, `POLineQty`) VALUES
(1, 1, 1, 10, 200),
(2, 1, 2, 12, 50),
(3, 2, 1, 10, 200),
(4, 2, 2, 12, 50);

--
-- Dumping data for table `prescription`
--

INSERT INTO `prescription` (`PrescriptionID`, `OrderID`, `PatientID`) VALUES
(35, 88, 2),
(34, 87, 2),
(33, 0, 1),
(32, 82, 1),
(31, 88, 3),
(30, 82, 2),
(29, 86, 3),
(28, 0, 3),
(27, 83, 1),
(26, 83, 2),
(25, 83, 3),
(24, 82, 3),
(23, 82, 3),
(22, 82, 1),
(21, 85, 1),
(20, 82, 3),
(17, 50, 2),
(18, 83, 1),
(19, 84, 3),
(36, 88, 1),
(37, 88, 3),
(38, 88, 3),
(39, 82, 3),
(40, 88, 3),
(41, 0, 1),
(42, 89, 3),
(43, 90, 1),
(44, 0, 3),
(45, 93, 2),
(46, 95, 1),
(47, 96, 2),
(48, 88, 2),
(49, 97, 2),
(50, 98, 3);

--
-- Dumping data for table `prescriptionline`
--

INSERT INTO `prescriptionline` (`PrescriptionLineID`, `PrescriptionID`, `MedicalID`, `PrescriptionLineQty`, `PrescriptionLineMethod`, `LotID`) VALUES
(21, 23, 4, 1, 'eat', 2324),
(20, 18, 2, 1, 'eat', 234),
(19, 0, 1, 123, 'eat', 2342),
(18, 20, 3, 1, 'eat', 2342),
(17, 23, 3, 1, 'eat', 23423),
(16, 20, 4, 123, 'eat', 234242),
(15, 21, 3, 123, 'eat', 234234),
(14, 20, 4, 12, 'eat', 32424),
(11, 17, 7, 342, 'swallow', 23424),
(12, 17, 4, 1, 'eat', 324242),
(13, 18, 1, 11, 'drink', 234423),
(22, 0, 2, 2, 'eat', 0),
(23, 18, 3, 0, '', 0),
(24, 18, 2, 2342, 'eat', 2342),
(25, 18, 1, 123, '', 0),
(26, 18, 4, 0, 'eat', 0),
(27, 18, 4, 0, '', 0),
(28, 18, 7, 1111, 'eat', 123),
(29, 20, 8, 1231, 'eat', 2342),
(30, 27, 7, 123, 'eat', 2342),
(31, 28, 7, 100, 'drink', 122342),
(32, 29, 6, 111, 'eat', 12123),
(33, 29, 4, 11, 'eat', 23424),
(34, 25, 1, 123, 'eat', 234),
(35, 31, 2, 11, 'eat', 123),
(36, 31, 7, 5, 'eat', 2342),
(37, 32, 1, 1, '', 123456),
(38, 31, 3, 111, 'eat', 2342),
(39, 40, 5, 12, 'eat', 234),
(40, 42, 5, 12, 'eat', 234),
(41, 43, 4, 1, 'eat', 234),
(42, 20, 3, 23, 'eat', 3432),
(43, 45, 2, 123, 'eat', 234),
(44, 47, 3, 16, 'eat', 324),
(45, 42, 5, 10, 'Eating', 98745),
(46, 26, 8, 2, 'Eat', 963175),
(47, 49, 7, 2, 'Eat', 933175),
(48, 50, 3, 12, 'eat', 234234);

--
-- Dumping data for table `receivehdr`
--

INSERT INTO `receivehdr` (`ReceiveHDRID`, `POID`, `EmployeeID`, `SupplierID`, `ReceiveCreateTime`, `ReceiveLastEditTime`, `ReceiveTotalAmount`) VALUES
(1, 1, 0, 1, '2013-12-04 00:00:00', '2013-12-18 00:00:00', 11),
(2, 2, 0, 2, '2013-12-09 00:00:00', '2013-12-27 00:00:00', 12),
(3, 3, 0, 3, '2013-12-23 00:00:00', '2013-12-31 00:00:00', 13);

--
-- Dumping data for table `receiveline`
--

INSERT INTO `receiveline` (`ReceiveLineID`, `medical_MedicalID`, `receivehdr_ReceiveHDRID`, `ReceiveLinePrice`, `ReceiveLineQty`, `ReceiveLotNo`) VALUES
(1, 1, 1, 300, 10, 23234),
(2, 2, 1, 1300, 10, 44344),
(3, 1, 2, 300, 10, 23234),
(4, 2, 2, 1300, 10, 44344);

--
-- Dumping data for table `suppliers`
--

INSERT INTO `suppliers` (`SupplierID`, `SupplierContactPerson`, `SupplierTel`, `SupplierAddress`, `SupplierStatus`, `SupplierName`) VALUES
(1, 'alex', '5436563', '333 road', '1', 'Dragon Company'),
(2, 'ben', '34535345', 'abc road hk', '0', 'Potenial Company'),
(3, 'cat', '5645643', 'def road hk', '1', 'Golden Company');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
