
class Order 
{
	Prescription Prescriptions[];
	Payment Payments[];
	
	int 	$OrderID;//*
	string 	$SalesDate;
	int 	$CustomerID;//
	int 	$SalesStatus;
	int 	$TotalPrice;//+
	
	public function __construct($OrderID, $SalesDate, $CustomerID, $SalesStatus)
	{
		$this->OrderID = $OrderID;
		$this->Salesdate = $SalesDate;
		$this->CustomerID = $CustomerID;
		$this->SalesStatus = $SalesStatus;
		$this->TotalPrice = $TotalPrice;//
	}
	
	function addPrescription($PrescriptionID, $this->OrderID, $PatientID, $PrescriptonStatus)
	{
		Prescriptions[] = new Prescription($PrescriptionID, $this->OrderID, $PatientID, $PrescriptonStatus);
	}
}

class Precription
{
	Item 	$Items[];
	
	int 	$PrescriptionID;//*
	int		$OrderID;//
	int 	$PatientID;//
	int 	$PrescriptionStaus;
	int 	$PrescriptionRemainDays;
	
}

class Item
{
	Lot		$Lots[];
	
	int 	$ItemID;//*
	int		$PrescriptionID;//
	string 	$ItemName
	string 	$ItemDescription;
	int 	$ItemCost;
	int 	$ItemRetailPrice;
	int 	$ItemStatus
	int 	$LotID;
	int 	$Qty;
}

class Lot
{
	int 	$LotID;//*
	int		$ItemID;//
	string	$ExpiredDate;
	int		$LotQty;
}

class Payment
{
	int 	$PaymentID;//*
	int		$OrderID;//
	int 	$$PaymentType;
	int 	$Aount;
}

class Customer
{
	int 	$CustomerNumber;//*
	string	$ClientName;
	int		$ClientStatus;
	string	$Telephone;
	string	$Address;
	string	$ContactPerson;
	int		$Quote;
	int		$Discount;
	string	$DeliveryTeam;
}

class Patient
{
	int		$PatientID;//*
	int 	$CustomerNumber;//
	int		$PatientStatus;
	int		$LocationID;//
	string	$FirstName;
	string	$LastName;
	int		$Gender;
	string	$DateOfBirth;
	string	$Remark;
	string	$Telephone;
	string	$Room;
	string	$Address;
}

class Location
{
	int		$LocationID;//*
	int		$CustomerID;//
	string	$LocationFacilityName;
	string	$Address;
}

class User
{
	int 	$UserID;//*
	string	$LoginName;
	string	$DateOfBirth;
	int		$Gender;
	string	$Phone;
	string	$Address;
	string	$Email;
	int		$Salary;
	string	$Position;
	int		$Status
}