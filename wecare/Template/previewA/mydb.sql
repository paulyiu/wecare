-- phpMyAdmin SQL Dump
-- version 4.0.4.2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Nov 25, 2013 at 04:49 PM
-- Server version: 5.6.13
-- PHP Version: 5.4.17

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `mydb`
--
-- CREATE DATABASE IF NOT EXISTS `mydb` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
-- USE `b31_13867109_db`;

-- --------------------------------------------------------

--
-- Table structure for table `clients`
--

DROP TABLE IF EXISTS `clients`;
CREATE TABLE IF NOT EXISTS `clients` (
  `ClientID` int(11) NOT NULL COMMENT 'Client ID (PK)',
  `ClientCompanyName` varchar(45) DEFAULT NULL COMMENT 'Client company name',
  `ClientTelephone` varchar(45) DEFAULT NULL COMMENT 'Client contact number',
  `ClientContactPerson` varchar(45) DEFAULT NULL COMMENT 'Client contact person',
  `ClientDiscount` float DEFAULT NULL COMMENT 'Client discount rate',
  `ClientQuota` float DEFAULT NULL COMMENT 'Client quota limit',
  `ClientStatus` int(11) DEFAULT NULL COMMENT 'Client status',
  `Address` varchar(100) DEFAULT NULL,
  `DeliveryTeam` varchar(16) DEFAULT NULL COMMENT 'Delivery Team',
  PRIMARY KEY (`ClientID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='Client record, ClientID is the primary key';

--
-- Dumping data for table `clients`
--

INSERT INTO `clients` (`ClientID`, `ClientCompanyName`, `ClientTelephone`, `ClientContactPerson`, `ClientDiscount`, `ClientQuota`, `ClientStatus`, `Address`, `DeliveryTeam`) VALUES
(1, 'ABC Company', '8888888', 'Otto', 0.02, 200, 1, 'HK', 'Team A'),
(2, 'DEF Company', '88887777', 'Jacob', 0.05, 2000, 1, 'HK', 'Team A'),
(3, 'GHI Company', '88886666', 'Larry', 0.1, 4000, 0, 'KL', 'Team B');

-- --------------------------------------------------------

--
-- Table structure for table `deliveryhdr`
--

DROP TABLE IF EXISTS `deliveryhdr`;
CREATE TABLE IF NOT EXISTS `deliveryhdr` (
  `DeliveryID` int(11) NOT NULL DEFAULT '0' COMMENT 'Delivery ID (PK)',
  `OrderID` int(11) NOT NULL COMMENT 'Order ID (FK)',
  `ClientID` int(11) NOT NULL COMMENT 'Client ID (FK)',
  `LocationID` int(11) NOT NULL COMMENT 'Location ID (FK)',
  `EmployeeID` int(11) NOT NULL COMMENT 'Employee ID (FK)',
  `DeliveryCreateTime` datetime DEFAULT NULL COMMENT 'Create time',
  `DeliveryLastEditTime` datetime DEFAULT NULL COMMENT 'Last modify time',
  `DeliveryExpectDate` datetime DEFAULT NULL COMMENT 'Exprect delivery time',
  `DeliveryTotalPrice` float DEFAULT NULL COMMENT 'Delivery total price',
  PRIMARY KEY (`DeliveryID`),
  KEY `fk_deliveryhdr_order1_idx` (`OrderID`),
  KEY `fk_deliveryhdr_clients1_idx` (`ClientID`),
  KEY `fk_deliveryhdr_locations1_idx` (`LocationID`),
  KEY `fk_deliveryhdr_employees1_idx` (`EmployeeID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='Delivery note header, DeliveryID is the primary key, OrderID is foreign key linked to Orders table, ClientID is foreign key linked to Clients table, LocationID is foreign key linked to Locations table, DeliveryEmployeeID is foreign key linked to Employees table';

-- --------------------------------------------------------

--
-- Table structure for table `deliveryline`
--

DROP TABLE IF EXISTS `deliveryline`;
CREATE TABLE IF NOT EXISTS `deliveryline` (
  `DeliveryLineID` int(11) NOT NULL COMMENT 'Delivery ID (PK)',
  `DeliveryID` int(11) NOT NULL COMMENT 'Delivery ID (FK)',
  `MedicalID` int(11) NOT NULL COMMENT 'Medical ID (FK)',
  `LotID` int(11) NOT NULL COMMENT 'Lot ID (FK)',
  `DeliveryLineQty` int(11) DEFAULT NULL COMMENT 'Quantity',
  `DeliveryLineItemPrice` float DEFAULT NULL COMMENT 'Line item amount',
  PRIMARY KEY (`DeliveryLineID`),
  KEY `fk_deliveryline_deliveryhdr1_idx` (`DeliveryID`),
  KEY `fk_deliveryline_medical1_idx` (`MedicalID`),
  KEY `fk_deliveryline_lots1_idx` (`LotID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='Delivery detail, DeliveryLineID is the primary key of DeliveryLine, Medicines is foreign key linked to Medicines table';

-- --------------------------------------------------------

--
-- Table structure for table `employees`
--

DROP TABLE IF EXISTS `employees`;
CREATE TABLE IF NOT EXISTS `employees` (
  `EmployeeID` int(11) NOT NULL DEFAULT '0' COMMENT 'Employee ID (PK)',
  `EmployeeFirstName` varchar(45) DEFAULT NULL COMMENT 'Employee first name',
  `EmployeeLastName` varchar(45) DEFAULT NULL COMMENT 'Employee last name',
  `EmployeeTitle` varchar(45) DEFAULT NULL COMMENT 'Employee Title',
  `Login` varchar(45) DEFAULT NULL COMMENT 'Login name',
  `Password` varchar(45) DEFAULT NULL COMMENT 'Password',
  `EmployeeStatus` int(11) DEFAULT NULL COMMENT 'Employee status',
  `Gender` varchar(1) NOT NULL,
  `Phone` varchar(8) NOT NULL,
  `Address` varchar(100) NOT NULL,
  `Email` varchar(50) NOT NULL,
  `Salary` int(9) NOT NULL,
  PRIMARY KEY (`EmployeeID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='Employees record, EmployeeID is the primary key of Employees table';

--
-- Dumping data for table `employees`
--

INSERT INTO `employees` (`EmployeeID`, `EmployeeFirstName`, `EmployeeLastName`, `EmployeeTitle`, `Login`, `Password`, `EmployeeStatus`, `Gender`, `Phone`, `Address`, `Email`, `Salary`) VALUES
(0, 'Admin', 'Super', 'Site admin', 'admin', '21232f297a57a5a743894a0e4a801fc3', 1, '', '', '', '', 0);

-- --------------------------------------------------------

--
-- Table structure for table `locations`
--

DROP TABLE IF EXISTS `locations`;
CREATE TABLE IF NOT EXISTS `locations` (
  `LocationID` int(11) NOT NULL COMMENT 'Location ID (PK)',
  `ClientID` int(11) NOT NULL COMMENT 'Client ID (FK)',
  `LocationFacilityName` varchar(45) DEFAULT NULL COMMENT 'Location description',
  `LocationFloor` varchar(45) DEFAULT NULL COMMENT 'Location floor',
  `LocationWingNo` varchar(45) DEFAULT NULL COMMENT 'Location wing number',
  `LocationBuilding` varchar(255) DEFAULT NULL COMMENT 'Location building',
  `LocationRoad` varchar(45) DEFAULT NULL COMMENT 'Location road',
  `LocationCountry` varchar(45) DEFAULT NULL COMMENT 'Location country',
  `LocationStatus` int(11) DEFAULT NULL COMMENT 'Location status',
  PRIMARY KEY (`LocationID`),
  KEY `fk_locations_clients1_idx` (`ClientID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='Location detail for clients, LocationID is the primary key of Locations table, ClientID is foreign key linked to Clients table';

--
-- Dumping data for table `locations`
--

INSERT INTO `locations` (`LocationID`, `ClientID`, `LocationFacilityName`, `LocationFloor`, `LocationWingNo`, `LocationBuilding`, `LocationRoad`, `LocationCountry`, `LocationStatus`) VALUES
(1, 1, 'Union Hospital', '', NULL, 'LG/F,logistic Department,1 union road, kwun tong', '', NULL, NULL),
(2, 2, 'Tseng Kwun O Hospital', NULL, NULL, '2/F,Logistic Department, 2 choi ming st,tseng kwun O', NULL, NULL, NULL),
(3, 3, 'Elisablish Hosptial', NULL, NULL, '1/F, 123 golf road, jordan', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `lots`
--

DROP TABLE IF EXISTS `lots`;
CREATE TABLE IF NOT EXISTS `lots` (
  `LotID` int(11) NOT NULL COMMENT 'Lot ID (PK)',
  `MedicalID` int(11) NOT NULL COMMENT 'Medical ID (FK)',
  `LotsQty` int(11) DEFAULT NULL COMMENT 'Quantity',
  `LotsExpireDate` datetime DEFAULT NULL COMMENT 'Expire Date',
  PRIMARY KEY (`LotID`),
  KEY `fk_lots_medical1_idx` (`MedicalID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='Lot record for medicines, LotID is the primary key of Lots table, MedicineID is foreign key linked to Medicines table';

-- --------------------------------------------------------

--
-- Table structure for table `medical`
--

DROP TABLE IF EXISTS `medical`;
CREATE TABLE IF NOT EXISTS `medical` (
  `MedicalID` int(11) NOT NULL COMMENT 'Medical ID (PK)',
  `MedicalName` varchar(45) DEFAULT NULL COMMENT 'Medical name',
  `MedicalDescription` varchar(45) DEFAULT NULL COMMENT 'Medical description',
  `MedicalPrice` float DEFAULT NULL COMMENT 'Medical price',
  `MedicalRetailPrice` float DEFAULT NULL COMMENT 'Medicine retail price',
  `MedicalStatus` int(11) DEFAULT NULL COMMENT 'Medicine status',
  `MedicalTotalQty` int(11) DEFAULT NULL COMMENT 'Medical total quantity',
  `MedicalType` varchar(45) DEFAULT NULL COMMENT 'Drag type',
  PRIMARY KEY (`MedicalID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='Item details record, MedicineID is the primary key of Item table';

--
-- Dumping data for table `medical`
--

INSERT INTO `medical` (`MedicalID`, `MedicalName`, `MedicalDescription`, `MedicalPrice`, `MedicalRetailPrice`, `MedicalStatus`, `MedicalTotalQty`, `MedicalType`) VALUES
(1, 'arthralgia', 'arthralgia', 10, 20, 1, 200, '7569841'),
(2, 'paresthesia', 'paresthesia', 12, 25, 1, 50, '7569841'),
(3, 'dyspepsia', 'dyspepsia', 19, 45, 0, 50, '7569841'),
(4, 'Panadol', 'Panadol', 10, 20, 1, 50, '2378652'),
(5, 'Anti-cancer drugs', 'Anti-cancer drugs', 50, 80, 1, 150, '2378652');

-- --------------------------------------------------------

--
-- Table structure for table `order`
--

DROP TABLE IF EXISTS `order`;
CREATE TABLE IF NOT EXISTS `order` (
  `OrderID` int(11) NOT NULL COMMENT 'Order ID (PK)',
  `EmployeeID` int(11) NOT NULL COMMENT 'Employee ID (FK)',
  `ClientID` int(11) NOT NULL COMMENT 'Client ID (FK)',
  `LocationID` int(11) NOT NULL COMMENT 'Location ID (FK)',
  `PaymentID` int(11) DEFAULT NULL COMMENT 'Order payment ID (FK)',
  `OrderPackingEmpID` int(11) DEFAULT NULL COMMENT 'Employee ID for packing (FK)',
  `OrderFinalCheckEmpID` int(11) DEFAULT NULL COMMENT 'Employee ID for final check (FK)',
  `OrderCreateTime` datetime DEFAULT NULL COMMENT 'Create time',
  `OrderLastEditTime` datetime DEFAULT NULL COMMENT 'Last modify time',
  `OrderExpectDeliveryDate` datetime DEFAULT NULL COMMENT 'Expect delivery time',
  `OrderStatus` int(11) DEFAULT NULL COMMENT 'Order status (0: cancel; 1:complete; 2:inactive)',
  `OrderTotalPrice` float DEFAULT NULL COMMENT 'Order tatal price',
  PRIMARY KEY (`OrderID`),
  KEY `fk_order_employees1_idx` (`EmployeeID`),
  KEY `fk_order_clients1_idx` (`ClientID`),
  KEY `fk_order_locations1_idx` (`LocationID`),
  KEY `fk_order_payment1_idx` (`PaymentID`),
  KEY `fk_order_employees2_idx` (`OrderPackingEmpID`),
  KEY `fk_order_employees3_idx` (`OrderFinalCheckEmpID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='Order details, OrderID is the primary key of OrderHdr table, ClientID is foreign key linked to Clients table, LocationID is foreign key linked to Locations table, EmployeeID is foreign key linked to Employees table';

--
-- Dumping data for table `order`
--

INSERT INTO `order` (`OrderID`, `EmployeeID`, `ClientID`, `LocationID`, `PaymentID`, `OrderPackingEmpID`, `OrderFinalCheckEmpID`, `OrderCreateTime`, `OrderLastEditTime`, `OrderExpectDeliveryDate`, `OrderStatus`, `OrderTotalPrice`) VALUES
(1, 0, 1, 1, NULL, NULL, NULL, '2013-11-04 00:00:00', NULL, NULL, 1, 15004),
(2, 0, 2, 2, NULL, NULL, NULL, '2013-11-05 00:00:00', NULL, NULL, 1, 12520),
(3, 0, 3, 3, NULL, NULL, NULL, '2013-11-06 00:00:00', NULL, NULL, 2, 8080);

-- --------------------------------------------------------

--
-- Table structure for table `patient`
--

DROP TABLE IF EXISTS `patient`;
CREATE TABLE IF NOT EXISTS `patient` (
  `PatientID` int(11) NOT NULL COMMENT 'Patient ID (PK)',
  `ClientID` int(11) NOT NULL COMMENT 'Client ID (FK)',
  `LocationID` int(11) NOT NULL COMMENT 'Patient location (FK)',
  `PatientFirstName` varchar(45) DEFAULT NULL COMMENT 'Patient first name',
  `PatientLastName` varchar(45) DEFAULT NULL COMMENT 'Patient last name',
  `PatientGender` int(11) DEFAULT NULL COMMENT 'Patient gender',
  `PatientDateOfBirth` date DEFAULT NULL COMMENT 'Patient date of birth',
  `PatientRemark` varchar(45) DEFAULT NULL COMMENT 'Patient remark',
  `PatientContact` varchar(45) DEFAULT NULL COMMENT 'Patient contact',
  `PatientRoom` varchar(45) DEFAULT NULL COMMENT 'Patient room number',
  `PatientAddress` varchar(45) DEFAULT NULL COMMENT 'Patient address',
  `PatientStatus` int(11) DEFAULT NULL COMMENT 'Patient status',
  PRIMARY KEY (`PatientID`),
  KEY `fk_patient_clients1_idx` (`ClientID`),
  KEY `fk_patient_locations1_idx` (`LocationID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='Keep patient record, patientID is the primary key of Patient table, clientID is foreign key linked to Clients table, LocationID is foreign key linked to Locations table';

--
-- Dumping data for table `patient`
--

INSERT INTO `patient` (`PatientID`, `ClientID`, `LocationID`, `PatientFirstName`, `PatientLastName`, `PatientGender`, `PatientDateOfBirth`, `PatientRemark`, `PatientContact`, `PatientRoom`, `PatientAddress`, `PatientStatus`) VALUES
(1, 1, 1, 'Veronica', 'Fung', 0, '2013-11-05', NULL, '77778888', '4401', 'HK', 1),
(2, 2, 2, 'Raymond', 'Chan', 1, '2013-11-13', NULL, '77779999', '3403', 'KL', 0),
(3, 3, 3, 'Rita', 'Tse', 0, '2013-11-13', NULL, '77779999', '1011', 'KL', 1);

-- --------------------------------------------------------

--
-- Table structure for table `payment`
--

DROP TABLE IF EXISTS `payment`;
CREATE TABLE IF NOT EXISTS `payment` (
  `PaymentID` int(11) NOT NULL COMMENT 'Payment ID (PK)',
  `ClientID` int(11) NOT NULL COMMENT 'Client ID (FK)',
  `EmployeeID` int(11) NOT NULL COMMENT 'EmployeeID (FK)',
  `PrescriptionID` int(11) NOT NULL COMMENT 'Prescription ID (FK)',
  `PaymentCreateTime` datetime DEFAULT NULL COMMENT 'Payment create time',
  `PaymentLastEditTime` datetime DEFAULT NULL COMMENT 'Payment last modify time',
  `PaymentType` int(11) DEFAULT NULL COMMENT 'Payment type',
  `PaymentAmount` float DEFAULT NULL COMMENT 'Payment amount',
  PRIMARY KEY (`PaymentID`),
  KEY `fk_payment_clients1_idx` (`ClientID`),
  KEY `fk_payment_employees1_idx` (`EmployeeID`),
  KEY `fk_payment_prescription1_idx` (`PrescriptionID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='Keep payment transaction record, PaymentID is the primary key, ClientID is foreign key linked to Clients table, EmployeeID is foreign key linked to Employees table';

--
-- Dumping data for table `payment`
--

INSERT INTO `payment` (`PaymentID`, `ClientID`, `EmployeeID`, `PrescriptionID`, `PaymentCreateTime`, `PaymentLastEditTime`, `PaymentType`, `PaymentAmount`) VALUES
(1, 1, 0, 1, '2013-11-05 00:00:00', '2013-11-20 00:00:00', 1, 250),
(2, 2, 0, 2, '2013-11-05 00:00:00', '2013-11-05 00:00:00', 1, 250),
(3, 3, 0, 3, '2013-11-05 00:00:00', '2013-11-05 00:00:00', 2, 800);

-- --------------------------------------------------------

--
-- Table structure for table `pohdr`
--

DROP TABLE IF EXISTS `pohdr`;
CREATE TABLE IF NOT EXISTS `pohdr` (
  `POID` int(11) NOT NULL COMMENT 'PO ID (PK)',
  `SupplierID` int(11) NOT NULL COMMENT 'Supplier ID (FK)',
  `EmployeeID` int(11) NOT NULL COMMENT 'Employee ID (FK)',
  `POCreateTime` datetime DEFAULT NULL COMMENT 'Create Time',
  `POLastEditTime` datetime DEFAULT NULL COMMENT 'Last modify time',
  `POExpectDelivery` datetime DEFAULT NULL COMMENT 'Expected delivery time',
  `POTotal` float DEFAULT NULL COMMENT 'Total amount of the PO',
  `POStatus` int(11) DEFAULT NULL COMMENT 'PO status',
  PRIMARY KEY (`POID`),
  KEY `fk_pohdr_suppliers1_idx` (`SupplierID`),
  KEY `fk_pohdr_employees1_idx` (`EmployeeID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='Purchase order header record, POHDR_ID is the primary key of POhdr table, SupplierID is foreign key linked to Suppliers table, EmployeeID is foreign key linked to Employees table';

--
-- Dumping data for table `pohdr`
--

INSERT INTO `pohdr` (`POID`, `SupplierID`, `EmployeeID`, `POCreateTime`, `POLastEditTime`, `POExpectDelivery`, `POTotal`, `POStatus`) VALUES
(1, 1, 0, '2013-11-04 00:00:00', '2013-11-05 00:00:00', '2013-11-18 00:00:00', 20, 3),
(2, 2, 0, '2013-11-04 00:00:00', '2013-11-05 00:00:00', '2013-11-19 00:00:00', 22, 3),
(3, 3, 0, '2013-11-05 00:00:00', '2013-11-06 00:00:00', '2013-11-19 00:00:00', 25, 2);

-- --------------------------------------------------------

--
-- Table structure for table `poline`
--

DROP TABLE IF EXISTS `poline`;
CREATE TABLE IF NOT EXISTS `poline` (
  `POLineID` int(11) NOT NULL COMMENT 'PO Line ID (PK)',
  `POID` int(11) NOT NULL COMMENT 'PO ID (FK)',
  `MedicalID` int(11) NOT NULL COMMENT 'PO item id (FK)',
  `POLinePrice` float DEFAULT NULL COMMENT 'Purchase line price',
  `POLineQty` int(11) DEFAULT NULL COMMENT 'Purchase line quility',
  PRIMARY KEY (`POLineID`),
  KEY `fk_poline_pohdr1_idx` (`POID`),
  KEY `fk_poline_medical1_idx` (`MedicalID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='Purchase order detail record, LineID is the primary key of POLine table, POID is foreign key linked to POHdr table';

-- --------------------------------------------------------

--
-- Table structure for table `prescription`
--

DROP TABLE IF EXISTS `prescription`;
CREATE TABLE IF NOT EXISTS `prescription` (
  `PrescriptionID` int(11) NOT NULL COMMENT 'Prescription ID (PK)',
  `OrderID` int(11) NOT NULL COMMENT 'Order ID (FK)',
  `PatientID` int(11) NOT NULL COMMENT 'Patient ID (FK)',
  PRIMARY KEY (`PrescriptionID`),
  KEY `fk_prescription_order1_idx` (`OrderID`),
  KEY `fk_prescription_patient1_idx` (`PatientID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='Prescriptionrecord, PrescriptionID is the primary key of Prescription table, OrderID is foreign key linked to Order table, PatientID is foreign key linked to Patient table';

--
-- Dumping data for table `prescription`
--

INSERT INTO `prescription` (`PrescriptionID`, `OrderID`, `PatientID`) VALUES
(1, 1, 1),
(2, 2, 2),
(3, 3, 3);

-- --------------------------------------------------------

--
-- Table structure for table `prescriptionline`
--

DROP TABLE IF EXISTS `prescriptionline`;
CREATE TABLE IF NOT EXISTS `prescriptionline` (
  `PrescriptionLineID` int(11) NOT NULL COMMENT 'Prescription line ID (PK)',
  `PrescriptionID` int(11) NOT NULL COMMENT 'Prescription ID (FK)',
  `MedicalID` int(11) NOT NULL COMMENT 'Medical ID (FK)',
  `PrescriptionLineQty` int(11) DEFAULT NULL COMMENT 'Quantity',
  `PrescriptionLineMethod` varchar(45) DEFAULT NULL COMMENT 'Prescription Method',
  PRIMARY KEY (`PrescriptionLineID`),
  KEY `fk_prescriptionline_prescription1_idx` (`PrescriptionID`),
  KEY `fk_prescriptionline_medical1_idx` (`MedicalID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='Prescription line detail, PrescriptionLineID is the primary key of PrescriptionLine, PrescriptionID is foreign key to Prescription, MedicalID is foreign key linked to Medicines table';

--
-- Dumping data for table `prescriptionline`
--

INSERT INTO `prescriptionline` (`PrescriptionLineID`, `PrescriptionID`, `MedicalID`, `PrescriptionLineQty`, `PrescriptionLineMethod`) VALUES
(1, 1, 1, 2, NULL),
(2, 1, 2, 1, NULL),
(3, 2, 3, 3, NULL),
(4, 2, 4, 5, NULL),
(5, 3, 5, 10, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `receivehdr`
--

DROP TABLE IF EXISTS `receivehdr`;
CREATE TABLE IF NOT EXISTS `receivehdr` (
  `ReceiveHDRID` int(11) NOT NULL COMMENT 'Receive ID (PK)',
  `POID` int(11) NOT NULL COMMENT 'Purchase order ID (FK)',
  `EmployeeID` int(11) NOT NULL COMMENT 'Employee ID (FK)',
  `SupplierID` int(11) NOT NULL COMMENT 'Supplier ID (FK)',
  `ReceiveCreateTime` datetime DEFAULT NULL COMMENT 'Create time',
  `ReceiveLastEditTime` datetime DEFAULT NULL COMMENT 'Last modify time',
  `ReceiveTotalAmount` float DEFAULT NULL COMMENT 'Receive total amount',
  PRIMARY KEY (`ReceiveHDRID`),
  KEY `fk_receivehdr_employees1_idx` (`EmployeeID`),
  KEY `fk_receivehdr_suppliers1_idx` (`SupplierID`),
  KEY `fk_receivehdr_pohdr1_idx` (`POID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='Receive header that link information of order receive together, ReceiveID is the primary key of ReceiveHDR, POID is foreign key linked to PO';

--
-- Dumping data for table `receivehdr`
--

INSERT INTO `receivehdr` (`ReceiveHDRID`, `POID`, `EmployeeID`, `SupplierID`, `ReceiveCreateTime`, `ReceiveLastEditTime`, `ReceiveTotalAmount`) VALUES
(1, 1, 0, 1, NULL, NULL, NULL),
(2, 2, 0, 2, NULL, NULL, NULL),
(3, 3, 0, 3, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `receiveline`
--

DROP TABLE IF EXISTS `receiveline`;
CREATE TABLE IF NOT EXISTS `receiveline` (
  `ReceiveLineID` int(11) NOT NULL COMMENT 'Receive line ID (PK)',
  `medical_MedicalID` int(11) NOT NULL COMMENT 'Receive medical ID (FK)',
  `receivehdr_ReceiveHDRID` int(11) NOT NULL COMMENT 'Receive header ID (FK)',
  `ReceiveLinePrice` float DEFAULT NULL COMMENT 'Delivery note total price',
  `ReceiveLineQty` int(11) DEFAULT NULL COMMENT 'Receive line quitily',
  `ReceiveLotNo` int(11) DEFAULT NULL COMMENT 'Lot Number of receive line',
  PRIMARY KEY (`ReceiveLineID`),
  KEY `fk_receiveline_medical1_idx` (`medical_MedicalID`),
  KEY `fk_receiveline_receivehdr1_idx` (`receivehdr_ReceiveHDRID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='Delivery detail records and total price of receive line, ReceiveLineID is the primary key of ReceiveLine, LineID is foreign key linked to POLine table';

-- --------------------------------------------------------

--
-- Table structure for table `role`
--

DROP TABLE IF EXISTS `role`;
CREATE TABLE IF NOT EXISTS `role` (
  `RoleID` int(11) NOT NULL COMMENT 'Role ID (PK)',
  `EmployeeID` int(11) NOT NULL COMMENT 'Employee ID (FK)',
  `RoleName` varchar(45) DEFAULT NULL COMMENT 'Role name',
  `RoleDescription` varchar(45) DEFAULT NULL COMMENT 'Role description',
  PRIMARY KEY (`RoleID`),
  KEY `fk_role_employees_idx` (`EmployeeID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='For assign permission and restrict access to the system. RoleID is primary key of Role table, EmployeeID is foreign key linked to Employees table.';

-- --------------------------------------------------------

--
-- Table structure for table `suppliers`
--

DROP TABLE IF EXISTS `suppliers`;
CREATE TABLE IF NOT EXISTS `suppliers` (
  `SupplierID` int(11) NOT NULL COMMENT 'Supplier ID (PK)',
  `SupplierContactPerson` varchar(45) DEFAULT NULL COMMENT 'Supplier contact person',
  `SupplierTel` varchar(45) DEFAULT NULL COMMENT 'Supplier telephone',
  `SupplierAddress` varchar(45) DEFAULT NULL COMMENT 'Supplier address',
  `SupplierStatus` varchar(45) DEFAULT NULL COMMENT 'Supplier status',
  PRIMARY KEY (`SupplierID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='Keep supplier record and contact detail, supplierID is the primary key of Suppliers';

--
-- Dumping data for table `suppliers`
--

INSERT INTO `suppliers` (`SupplierID`, `SupplierContactPerson`, `SupplierTel`, `SupplierAddress`, `SupplierStatus`) VALUES
(1, 'Dragon Company', NULL, NULL, '1'),
(2, 'Potenial Company', NULL, NULL, '0'),
(3, 'Golden Company', NULL, NULL, '1');

--
-- Constraints for dumped tables
--

--
-- Constraints for table `deliveryhdr`
--
ALTER TABLE `deliveryhdr`
  ADD CONSTRAINT `fk_deliveryhdr_clients1` FOREIGN KEY (`ClientID`) REFERENCES `clients` (`ClientID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_deliveryhdr_employees1` FOREIGN KEY (`EmployeeID`) REFERENCES `employees` (`EmployeeID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_deliveryhdr_locations1` FOREIGN KEY (`LocationID`) REFERENCES `locations` (`LocationID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_deliveryhdr_order1` FOREIGN KEY (`OrderID`) REFERENCES `order` (`OrderID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `deliveryline`
--
ALTER TABLE `deliveryline`
  ADD CONSTRAINT `fk_deliveryline_deliveryhdr1` FOREIGN KEY (`DeliveryID`) REFERENCES `deliveryhdr` (`DeliveryID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_deliveryline_lots1` FOREIGN KEY (`LotID`) REFERENCES `lots` (`LotID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_deliveryline_medical1` FOREIGN KEY (`MedicalID`) REFERENCES `medical` (`MedicalID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `locations`
--
ALTER TABLE `locations`
  ADD CONSTRAINT `fk_locations_clients1` FOREIGN KEY (`ClientID`) REFERENCES `clients` (`ClientID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `lots`
--
ALTER TABLE `lots`
  ADD CONSTRAINT `fk_lots_medical1` FOREIGN KEY (`MedicalID`) REFERENCES `medical` (`MedicalID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `order`
--
ALTER TABLE `order`
  ADD CONSTRAINT `fk_order_clients1` FOREIGN KEY (`ClientID`) REFERENCES `clients` (`ClientID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_order_employees1` FOREIGN KEY (`EmployeeID`) REFERENCES `employees` (`EmployeeID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_order_employees2` FOREIGN KEY (`OrderPackingEmpID`) REFERENCES `employees` (`EmployeeID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_order_employees3` FOREIGN KEY (`OrderFinalCheckEmpID`) REFERENCES `employees` (`EmployeeID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_order_locations1` FOREIGN KEY (`LocationID`) REFERENCES `locations` (`LocationID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_order_payment1` FOREIGN KEY (`PaymentID`) REFERENCES `payment` (`PaymentID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `patient`
--
ALTER TABLE `patient`
  ADD CONSTRAINT `fk_patient_clients1` FOREIGN KEY (`ClientID`) REFERENCES `clients` (`ClientID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_patient_locations1` FOREIGN KEY (`LocationID`) REFERENCES `locations` (`LocationID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `payment`
--
ALTER TABLE `payment`
  ADD CONSTRAINT `fk_payment_clients1` FOREIGN KEY (`ClientID`) REFERENCES `clients` (`ClientID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_payment_employees1` FOREIGN KEY (`EmployeeID`) REFERENCES `employees` (`EmployeeID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_payment_prescription1` FOREIGN KEY (`PrescriptionID`) REFERENCES `prescription` (`PrescriptionID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `pohdr`
--
ALTER TABLE `pohdr`
  ADD CONSTRAINT `fk_pohdr_employees1` FOREIGN KEY (`EmployeeID`) REFERENCES `employees` (`EmployeeID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_pohdr_suppliers1` FOREIGN KEY (`SupplierID`) REFERENCES `suppliers` (`SupplierID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `poline`
--
ALTER TABLE `poline`
  ADD CONSTRAINT `fk_poline_medical1` FOREIGN KEY (`MedicalID`) REFERENCES `medical` (`MedicalID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_poline_pohdr1` FOREIGN KEY (`POID`) REFERENCES `pohdr` (`POID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `prescription`
--
ALTER TABLE `prescription`
  ADD CONSTRAINT `fk_prescription_order1` FOREIGN KEY (`OrderID`) REFERENCES `order` (`OrderID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_prescription_patient1` FOREIGN KEY (`PatientID`) REFERENCES `patient` (`PatientID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `prescriptionline`
--
ALTER TABLE `prescriptionline`
  ADD CONSTRAINT `fk_prescriptionline_medical1` FOREIGN KEY (`MedicalID`) REFERENCES `medical` (`MedicalID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_prescriptionline_prescription1` FOREIGN KEY (`PrescriptionID`) REFERENCES `prescription` (`PrescriptionID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `receivehdr`
--
ALTER TABLE `receivehdr`
  ADD CONSTRAINT `fk_receivehdr_employees1` FOREIGN KEY (`EmployeeID`) REFERENCES `employees` (`EmployeeID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_receivehdr_pohdr1` FOREIGN KEY (`POID`) REFERENCES `pohdr` (`POID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_receivehdr_suppliers1` FOREIGN KEY (`SupplierID`) REFERENCES `suppliers` (`SupplierID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `receiveline`
--
ALTER TABLE `receiveline`
  ADD CONSTRAINT `fk_receiveline_medical1` FOREIGN KEY (`medical_MedicalID`) REFERENCES `medical` (`MedicalID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_receiveline_receivehdr1` FOREIGN KEY (`receivehdr_ReceiveHDRID`) REFERENCES `receivehdr` (`ReceiveHDRID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `role`
--
ALTER TABLE `role`
  ADD CONSTRAINT `fk_role_employees` FOREIGN KEY (`EmployeeID`) REFERENCES `employees` (`EmployeeID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
