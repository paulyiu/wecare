<?php
$template_path = "Template/previewB/";

$timeout = 60 * 15;
$fingerprint = md5($_SERVER['REMOTE_ADDR'].$_SERVER['HTTP_USER_AGENT']);
$status = "";
$debug=false;
if($_SERVER["SERVER_NAME"]=="localhost")
{
$db_host = "127.0.0.1:3307";
$db_main = "mydb";
$db_user = "root";
$db_pass = "usbw";
} else {
define('DB_HOST', getenv('OPENSHIFT_MYSQL_DB_HOST'));
define('DB_PORT', getenv('OPENSHIFT_MYSQL_DB_PORT'));
define('DB_USER', getenv('OPENSHIFT_MYSQL_DB_USERNAME'));
define('DB_PASS', getenv('OPENSHIFT_MYSQL_DB_PASSWORD'));
define('DB_NAME', getenv('OPENSHIFT_GEAR_NAME'));

$db_host = constant("DB_HOST"); // Host name 
$db_port = constant("DB_PORT"); // Host port
$db_user = constant("DB_USER"); // Mysql username 
$db_pass = constant("DB_PASS"); // Mysql password 
$db_main = constant("DB_NAME"); // Database name 
}
$db = new mysqli($db_host.":".$db_port,$db_user,$db_pass,$db_main) or die();
$db->query("SET SESSION time_zone = '+8:00'");
/*$layout_file = $template_path."template.json";
$template_files;
if(file_exists($layout_file))
{
	$template_files = json_decode(file_get_contents($layout_file));
	//var_dump($template_files);
}else{
	echo "Cannot loading JSON file:$layout_file";
}*/
$menus;
$menu_file = $template_path."command.json";
if(file_exists($menu_file))
{
	$menus = json_decode(file_get_contents($menu_file));
}else{
	echo "Cannot loading JSON file:$menu_file";
}
$forms;
$forms_file = $template_path."forms.json";
if(file_exists($forms_file))
{
	$forms = json_decode(file_get_contents($forms_file));
}else{
	echo "Cannot loading JSON file:$forms_file";
}

	session_start();
	if (    (isset($_SESSION['last_active']) && $_SESSION['last_active']<(time()-$timeout))
	     || (isset($_SESSION['fingerprint']) && $_SESSION['fingerprint']!=$fingerprint)
	     || isset($_GET['logout'])
	     || (isset($_GET['page']) && $_GET['page']=='Logout')
	    )
	{
	   setcookie(session_name(), '', time()-3600, '/');
	   session_destroy();
	   unset($_SESSION["login"]);
	   echo "<script>document.location='/wecare/'</script>";
	   // 
	}
	session_regenerate_id();
	$_SESSION['last_active'] = time();
	$_SESSION['fingerprint'] = $fingerprint;


?>
